<?xml version="1.0" encoding="UTF-8"?>
<tileset name="tileset0" tilewidth="16" tileheight="16" tilecount="64">
 <image source="tileset_images/tiles0.png" width="256" height="64"/>
 <tile id="4">
  <properties>
   <property name="collider" value="true"/>
  </properties>
 </tile>
 <tile id="5">
  <properties>
   <property name="door" value="unlocked_1"/>
  </properties>
 </tile>
 <tile id="10">
  <properties>
   <property name="collider" value="true"/>
   <property name="door" value="locked_1"/>
  </properties>
 </tile>
 <tile id="12">
  <properties>
   <property name="collider" value="true"/>
  </properties>
 </tile>
 <tile id="13">
  <properties>
   <property name="collider" value="true"/>
  </properties>
 </tile>
 <tile id="16">
  <properties>
   <property name="collider" value="true"/>
  </properties>
 </tile>
 <tile id="17">
  <properties>
   <property name="step_trap" value="1"/>
  </properties>
 </tile>
 <tile id="19">
  <properties>
   <property name="step_trap" value="2"/>
  </properties>
 </tile>
 <tile id="21">
  <properties>
   <property name="step_trap" value="3"/>
  </properties>
 </tile>
 <tile id="23">
  <properties>
   <property name="step_trap" value="0"/>
  </properties>
 </tile>
 <tile id="25">
  <properties>
   <property name="collider" value="true"/>
   <property name="door" value="locked_0"/>
  </properties>
 </tile>
 <tile id="27">
  <properties>
   <property name="step_trap" value="4"/>
  </properties>
 </tile>
 <tile id="29">
  <properties>
   <property name="collider" value="true"/>
  </properties>
 </tile>
 <tile id="30">
  <properties>
   <property name="step_trap" value="5"/>
  </properties>
 </tile>
 <tile id="32">
  <properties>
   <property name="step_trap" value="6"/>
  </properties>
 </tile>
 <tile id="35">
  <properties>
   <property name="collider" value="true"/>
  </properties>
 </tile>
 <tile id="36">
  <properties>
   <property name="collider" value="true"/>
  </properties>
 </tile>
 <tile id="37">
  <properties>
   <property name="step_trap" value="7"/>
  </properties>
 </tile>
 <tile id="39">
  <properties>
   <property name="step_trap" value="8"/>
  </properties>
 </tile>
 <tile id="41">
  <properties>
   <property name="collider" value="true"/>
  </properties>
 </tile>
 <tile id="45">
  <properties>
   <property name="collider" value="true"/>
  </properties>
 </tile>
</tileset>
