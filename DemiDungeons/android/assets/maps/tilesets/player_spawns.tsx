<?xml version="1.0" encoding="UTF-8"?>
<tileset name="player_spawns" tilewidth="16" tileheight="16" tilecount="4">
 <image source="tileset_images/player_spawns.png" width="64" height="16"/>
 <tile id="0">
  <properties>
   <property name="player" value="warrior"/>
  </properties>
 </tile>
 <tile id="1">
  <properties>
   <property name="player" value="mage"/>
  </properties>
 </tile>
 <tile id="2">
  <properties>
   <property name="player" value="rogue"/>
  </properties>
 </tile>
 <tile id="3">
  <properties>
   <property name="player" value="hunter"/>
  </properties>
 </tile>
</tileset>
