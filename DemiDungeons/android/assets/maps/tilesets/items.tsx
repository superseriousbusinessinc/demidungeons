<?xml version="1.0" encoding="UTF-8"?>
<tileset name="items" tilewidth="16" tileheight="16" tilecount="128">
 <image source="tileset_images/items.png" width="128" height="256"/>
 <tile id="8">
  <properties>
   <property name="key" value="0"/>
  </properties>
 </tile>
 <tile id="9">
  <properties>
   <property name="key" value="1"/>
  </properties>
 </tile>
 <tile id="10">
  <properties>
   <property name="key" value="2"/>
  </properties>
 </tile>
</tileset>
