package com.ssbi.demidungeons.android;

import android.os.Bundle;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.ssbi.demidungeons.DemiDungeonsGame;
import com.ssbi.demidungeons.utils.DemiDungeonsConfig;

public class AndroidLauncher extends AndroidApplication {
	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
		DemiDungeonsConfig ddConfig = new DemiDungeonsConfig();
		ddConfig.emailManager = new AndroidEmailManager(this);
		initialize(new DemiDungeonsGame(ddConfig), config);
	}
}
