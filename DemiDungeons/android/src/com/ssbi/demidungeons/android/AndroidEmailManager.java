package com.ssbi.demidungeons.android;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import com.ssbi.demidungeons.utils.EmailManager;

/**
 * Created by Benjamin on 2015-12-02.
 */
public class AndroidEmailManager extends EmailManager {
    private Context ctx;

    public AndroidEmailManager(Context context) {
        ctx = context;
    }

    @Override
    public void sendEmail(String sender, String receiver, String subject, String message) {
        String [] to = {receiver};
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.putExtra(Intent.EXTRA_EMAIL, to);
        emailIntent.putExtra(Intent.EXTRA_TEXT, message);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
        emailIntent.setType("message/rfc822");
        ctx.startActivity(Intent.createChooser(emailIntent, "Email"));
    }
}
