package com.ssbi.demidungeons.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Disposable;

import java.util.HashMap;

/**
 * Created by Benjamin on 2015-10-24.
 */
public final class TextureManager implements Disposable {
    private static Texture tilesetTexture = null;
    private static Texture effectsTexture = null;
    private static Texture itemsTexture = null;
    private static Texture toolbarTexture = null;
    private static Texture buttonsTexture = null;
    private static HashMap<Utils.ItemType, TextureRegion> itemRegions = null;
    private static HashMap<Utils.EffectType, TextureRegion> effectRegions = null;
    private static HashMap<Utils.ToolbarType, TextureRegion> toolbarRegions = null;
    private static HashMap<Utils.ButtonsType, TextureRegion> buttonsRegions = null;

    public static Texture getTilesetTexture() {
        if (tilesetTexture == null)
            tilesetTexture = new Texture(Gdx.files.internal("images/tiles0.png"));
        return tilesetTexture;
    }

    public static TextureRegion getItemTexture(Utils.ItemType itemType) {
        if (itemsTexture == null)
            itemsTexture = new Texture(Gdx.files.internal("images/items.png"));
        if (itemRegions == null)
            itemRegions = new HashMap<Utils.ItemType, TextureRegion>();
        if (!itemRegions.containsKey(itemType)) {
            int w = Utils.TILE_WIDTH_INT;
            int h = Utils.TILE_HEIGHT_INT;
            switch (itemType) {
                case KEY_0:
                    itemRegions.put(itemType, new TextureRegion(itemsTexture, 0, 16, w, h));
                    break;
                case KEY_1:
                    itemRegions.put(itemType, new TextureRegion(itemsTexture, 16, 16, w, h));
                    break;
                case POTION_2:
                    itemRegions.put(itemType, new TextureRegion(itemsTexture, 16,112, w, h));
                    break;

            }
        }
        return itemRegions.get(itemType);
    }

    public static TextureRegion getEffectTexture(Utils.EffectType effectType) {
        if (effectsTexture == null)
            effectsTexture = new Texture(Gdx.files.internal("images/effects.png"));
        if (effectRegions == null)
            effectRegions = new HashMap<Utils.EffectType, TextureRegion>();
        if (!effectRegions.containsKey(effectType)) {
            switch (effectType) {
                case DAMAGE:
                    effectRegions.put(effectType, new TextureRegion(effectsTexture, 16, 8, 16, 8));
                    break;
            }
        }
        return effectRegions.get(effectType);
    }

    public static TextureRegion getToolbarTexture(Utils.ToolbarType toolbarType) {
        if (toolbarTexture == null)
            toolbarTexture = new Texture(Gdx.files.internal("images/toolbar.png"));
        if (toolbarRegions == null)
            toolbarRegions = new HashMap<Utils.ToolbarType, TextureRegion>();
        if (!toolbarRegions.containsKey(toolbarType)) {
            switch (toolbarType) {
                case INVENTORY:
                    toolbarRegions.put(toolbarType, new TextureRegion(toolbarTexture, 61, 7, 22, 25));
                    break;
                case INV_SLOT:
                    toolbarRegions.put(toolbarType, new TextureRegion(toolbarTexture, 83, 7, 22, 25));
                    break;
            }
        }
        return toolbarRegions.get(toolbarType);
    }

    public static TextureRegion getButtonsTexture(Utils.ButtonsType buttonsType) {
        if (buttonsTexture == null)
            buttonsTexture = new Texture(Gdx.files.internal("images/buttons.png"));
        if (buttonsRegions == null)
            buttonsRegions = new HashMap<Utils.ButtonsType, TextureRegion>();
        if (!buttonsRegions.containsKey(buttonsType)) {
            switch (buttonsType) {
                case BROWN:
                    buttonsRegions.put(buttonsType, new TextureRegion(buttonsTexture, 0, 0, 64, 32));
                    break;
                case RED:
                    buttonsRegions.put(buttonsType, new TextureRegion(buttonsTexture, 64, 0, 64, 32));
                    break;
                case GREEN:
                    buttonsRegions.put(buttonsType, new TextureRegion(buttonsTexture, 0, 32, 64, 32));
                    break;
                case YELLOW:
                    buttonsRegions.put(buttonsType, new TextureRegion(buttonsTexture, 64, 32, 64, 32));
                    break;
            }
        }
        return buttonsRegions.get(buttonsType);
    }

    @Override
    public void dispose() {
        TextureManager.itemsTexture.dispose();
    }
}
