package com.ssbi.demidungeons.utils;

/**
 * Created by Benjamin on 2015-12-02.
 */
public abstract class EmailManager {

    public EmailManager() {

    }

    public abstract void sendEmail(String sender, String receiver, String subject, String message);
}
