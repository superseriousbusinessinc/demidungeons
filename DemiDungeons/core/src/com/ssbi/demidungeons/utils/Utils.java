package com.ssbi.demidungeons.utils;

import com.badlogic.gdx.Gdx;

/**
 * Created by Benjamin on 2015-10-24.
 */
public class Utils {
    // Preference files
    public static String PREF_SAVE_FILE = "demi_dungeons.ddsav";
    public static String PREF_LEVEL_STATE = "dd_level_state.ddsav";
    // Preference values
    public static String VAL_LATEST_LEVEL = "latest_level";

    public static String VAL_CURRENT_TURN = "current_turn";

    public static String VAL_PLAYER_X = "player_x";

    public static String VAL_PLAYER_Y = "player_y";

    public static String VAL_PLAYER_HEALTH = "player_health";

    public static String VAL_ALIVE_ENEMIES = "alive_enemies";

    public static String VAL_PLAYER_INVENTORY_ITEM_NUM = "player_inventory_item_num";

    public static String VAL_ITEM_NUM = "item_num";


    public static String VAL_CURRENT_LEVEL = "current_level";

    public static String VAL_CURRENT_SCREEN = "current_screen";
    public static int SCREEN_MAIN = 0;
    public static int SCREEN_GAME = 1;

    // Tiles should always be 16 x 16 pixels.
    public static final int TILE_WIDTH_INT = 16;
    public static final int TILE_HEIGHT_INT = 16;
    public static final float TILE_WIDTH = 16.0f;
    public static final float TILE_HEIGHT = 16.0f;

    // Avatars have 9 inventory slots
    public static final int INV_SLOTS = 9;

    // Highest possible level
    public static final int HIGHEST_LEVEL = 10;

    // Player
    public enum AvatarType {
        P_WARRIOR,
        P_MAGE,
        P_ROGUE,
        P_HUNTER,
        E_RAT,
        E_RATQUEEN
    }
    // Items
    public enum ItemType {
        KEY_0,
        KEY_1,
        POTION_2
    }
    // Interactive Objects
    public enum InteractiveType {
        DOOR,
    }
    // Effects
    public enum EffectType {
        DAMAGE
    }
    // Toolbar
    public enum ToolbarType {
        INVENTORY,
        INV_SLOT
    }
    // Buttons
    public enum ButtonsType {
        BROWN,
        RED,
        GREEN,
        YELLOW
    }

    public static void log(String msg) {
        Gdx.app.log("DDApp", msg);
    }

    public static void err(String msg, Exception ex) {
        Gdx.app.error("DDApp", msg, ex);
    }

    public static void logD(String msg) {
        Gdx.app.debug("DDApp", msg);
    }
}
