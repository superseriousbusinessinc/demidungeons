package com.ssbi.demidungeons.objects;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.math.Vector2;
import com.ssbi.demidungeons.effects.OverlayEffect;
import com.ssbi.demidungeons.levels.GameLevel;
import com.ssbi.demidungeons.utils.TextureManager;
import com.ssbi.demidungeons.utils.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Benjamin on 2015-10-24.
 */
public abstract class Avatar extends WorldComponent {

    public enum State {
        IDLE,
        WALK,
        DEAD
    }

    protected GameLevel level;

    protected float speed = 100.0f;
    protected Utils.AvatarType type;
    protected State state;
    protected int health;

    protected float stateTime;

    protected Animation idleAnimation;
    protected Animation walkAnimation;
    protected Animation dieAnimation;

    protected TextureRegion currentFrame;

    protected Vector2 walkDestination;
    protected TiledMapTileLayer mainLayer;

    protected boolean isOrigPos = true;
    protected float originalX = 0.0f;
    protected float originalY = 0.0f;

    protected List<InventoryItem> inventory;

    protected int turnPoints = 1;
    protected int curTurnPoints;

    protected OverlayEffect oEffect;

    public Avatar(GameLevel level) {
        super();
        stateTime = 0.0f;
        inventory = new ArrayList<InventoryItem>();
        isCollider = true;
        oEffect = new OverlayEffect();
        this.level = level;
    }

    /**********************************
     * Abstract Methods               *
     **********************************/

    public abstract void damaged(int healthDamaged);
    public abstract void doTurn(GameLevel level);


    /**********************************
     * Public Methods                 *
     **********************************/

    public void update(float delta) {
        stateTime += delta;
        oEffect.update(delta);
    }

    @Override
    public void draw(Batch batch) {
        super.draw(batch);
        oEffect.draw(batch);
//        Sprite s = new Sprite(TextureManager.getEffectTexture(Utils.EffectType.DAMAGE));
//        s.setPosition(getX(), getY());
//        s.draw(batch);
    }

    public int getHealth() {
        return this.health;
    }

    public void setHealth(int health){
        this.health = health;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
        switch(state) {
            case DEAD:
                stateTime = 11.1f;
                break;
        }
    }

    public void setWalkDestination(Vector2 destination, TiledMapTileLayer mLayer) {
        if(getX() != destination.x || getY() != destination.y) {
            state = State.WALK;
            walkDestination = destination;
            stateTime = 0;
            mainLayer = mLayer;
        }
    }

    public List<InventoryItem> getInventory() {
        return inventory;
    }

    public void addInventoryItem(InventoryItem item) {
        inventory.add(item);
    }

    public void removeInventoryItem(InventoryItem item) {
        inventory.remove(item);
    }

    public InventoryItem getInventoryItem(Utils.ItemType itemType) {
        for (InventoryItem i : inventory) {
            if(i.getType() == itemType) {
                return i;
            }
        }
        return null;
    }

    /*
     * Purpose: used to hold the avatars original movement before the Avatar has moved, which is then used to
     * determine whether or not the avatar has moved a tile or not
     * Called: in the move method of the Player and Rat
     */
    public void setOriginalPos(float oldX,float oldY){
        if (isOrigPos == true) {
            originalX = oldX;
            originalY = oldY;
        }
        isOrigPos = false;
    }

    /*
     * Purpose: When the modulus is used, it will return the offset of how much the number is off.
     * Because the number was originally truncated by converting it to an int, a method needed to be created
     * to determine whether or not to round the number up or down
     * Called: in the move method of the Player and Rat
     */
    public int rounder(float pos) {
        int round = (int)(((int)pos)%Utils.TILE_WIDTH);
        if(round >= 8)
            pos += 16 - round;
        else
            pos -= round;

        return (int)pos;
    }

    /*
     * Purpose: To set the Avatar's position of the newly rounded numbers, to set the Avatar state to idle,
     * and set isOrigPos to true which will let the program know that it can call the setOriginalPos
     * if the player moves again
     * Called: in the move method of the Player and Rat
     */
    public void setGridPos(float x,float y){
        setPosition(x, y);
        state = State.IDLE;
        isOrigPos = true;
    }

    /*
     * Purpose: To determine which of the cartesian coordinates has changed from the
     * Avatar's original position. If the position hasn't changed, than there is no need to check it,
     * so just set it to null
     * Called: isColliding in Avatar
     */
    public TiledMapTileLayer.Cell isCartesianMovement(float pos, float origPos, TiledMapTileLayer.Cell cell)
    {
        if(pos != origPos)
            return cell;
        else
            return null;
    }

    public boolean isColliding(float newX, float newY) {
        float directions[] = {newY + 16, newY, newX, newX + 16, newX + 16,newY + 16, newX,newY + 16};//tile up, tile down, tile left, tile right
        for(int i = 0; i < directions.length; i++) {
            TiledMapTileLayer.Cell cell;
            if(i < 2)
                cell = isCartesianMovement(newY,originalY, mainLayer.getCell((int) ((newX + 8) / Utils.TILE_WIDTH), (int) ((directions[i]) / Utils.TILE_HEIGHT)));
            else if(i < 4)
                cell = isCartesianMovement(newX,originalX, mainLayer.getCell((int) ((directions[i]) / Utils.TILE_WIDTH), (int) ((newY + 8) / Utils.TILE_HEIGHT)));
            else
                if((cell = isCartesianMovement(newX,originalX, mainLayer.getCell((int) ((directions[i++]) / Utils.TILE_WIDTH), (int) ((directions[i]) / Utils.TILE_HEIGHT)))) != null
                        && (cell = isCartesianMovement(newY,originalY, mainLayer.getCell((int) ((directions[--i]) / Utils.TILE_WIDTH), (int) ((directions[++i]) / Utils.TILE_HEIGHT)))) != null)
                    cell = mainLayer.getCell((int) ((directions[--i]) / Utils.TILE_WIDTH), (int) ((directions[++i]) / Utils.TILE_HEIGHT));

            if (cell == null)
                continue;

            TiledMapTile tile = cell.getTile();
            MapProperties mp = tile.getProperties();
            if (mp.containsKey("collider")) {
                state = State.IDLE;
                return true;
            }
        }
        return false;
    }

    public void beginTurn() {
        curTurnPoints = turnPoints;
    }

    public void decreaseTurn(int actionCost) { curTurnPoints -= actionCost; }

    public void endTurn() {
        curTurnPoints = 0;
    }

    public boolean isTurnFinished() {
        return curTurnPoints <= 0;
    }

    public void showEffect(Utils.EffectType effectType, float seconds) {
        oEffect.setType(effectType);
        oEffect.setPosition(getX(), getY());
        oEffect.show(seconds, false, true);
    }

    public void showEffect(Utils.EffectType effectType, float seconds, float fadeDuration) {
        oEffect.setType(effectType);
        oEffect.setPosition(getX(), getY());
        oEffect.setFadeDuration(fadeDuration);
        oEffect.show(seconds, false, true);
    }

    public void showDamageEffect() {
        showEffect(Utils.EffectType.DAMAGE, 0.2f, 0.4f);
    }
}
