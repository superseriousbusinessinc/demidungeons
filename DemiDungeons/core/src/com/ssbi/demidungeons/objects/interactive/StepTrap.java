package com.ssbi.demidungeons.objects.interactive;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.ssbi.demidungeons.objects.Avatar;
import com.ssbi.demidungeons.objects.InventoryItem;
import com.ssbi.demidungeons.utils.TextureManager;
import com.ssbi.demidungeons.objects.WorldComponent;
import com.ssbi.demidungeons.utils.Utils;

/**
 * Created by AaronMurphy on 29/10/2015.
 */
public class StepTrap extends Interactive {
    protected static TextureRegion inactiveTex;
    protected TextureRegion trapTex;
    protected int trapType;
    protected boolean isActive;

    public StepTrap(int trapType, boolean isActive) {
        super();
        this.trapType = trapType;
        this.isActive = isActive;
        if (inactiveTex == null) {
            inactiveTex = new TextureRegion(
                    TextureManager.getTilesetTexture(),
                    Utils.TILE_WIDTH_INT * 7,
                    Utils.TILE_HEIGHT_INT * 1,
                    Utils.TILE_WIDTH_INT,
                    Utils.TILE_HEIGHT_INT
            );
        }
        int x;
        int y;

        switch (trapType) {
            case 0:
                x = 7;
                y = 1;
                break;
            case 1:
                x = 1;
                y = 1;
                break;
            case 2:
                x = 3;
                y = 1;
                break;
            case 3:
                x = 5;
                y = 1;
                break;
            case 4:
                x = 11;
                y = 1;
                break;
            case 5:
                x = 14;
                y = 1;
                break;
            case 6:
                x = 0;
                y = 2;
                break;
            case 7:
                x = 5;
                y = 2;
                break;
            case 8:
                x = 7;
                y = 2;
                break;
            default:
                x = 7;
                y = 1;
                break;
        }
        trapTex = new TextureRegion(
                TextureManager.getTilesetTexture(),
                Utils.TILE_WIDTH_INT * x,
                Utils.TILE_HEIGHT_INT * y,
                Utils.TILE_WIDTH_INT,
                Utils.TILE_HEIGHT_INT
        );
        if (isActive)
            setRegion(trapTex);
        else
            setRegion(inactiveTex);
        isCollider = false;
    }

    public void update(float delta) {

    }

    @Override
    public boolean onUseWith(InventoryItem item) {
        return false;
    }

    @Override
    public boolean onContact(WorldComponent component) {
        if (component instanceof Avatar) {
            if (isActive) {
                isActive = false;
                setRegion(inactiveTex);
                ((Avatar) component).damaged(50);
            }
            return true;
        }
        return false;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean isActive) {
        this.isActive = isActive;
        if (isActive)
            setRegion(trapTex);
        else
            setRegion(inactiveTex);
    }
}
