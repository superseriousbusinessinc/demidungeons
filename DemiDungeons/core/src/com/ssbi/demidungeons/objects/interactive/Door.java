package com.ssbi.demidungeons.objects.interactive;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.ssbi.demidungeons.objects.InventoryItem;
import com.ssbi.demidungeons.utils.TextureManager;
import com.ssbi.demidungeons.objects.WorldComponent;
import com.ssbi.demidungeons.utils.Utils;

/**
 * Created by Benjamin on 2015-10-25.
 */
public class Door extends Interactive {

    private boolean isLocked;

    private TextureRegion lockedTexture;
    private TextureRegion unlockedTexture;

    private Utils.ItemType unlockKeyType;

    public Door() {
        super();
        type = Utils.InteractiveType.DOOR;
        isCollider = true;

        // Load normal door textures.
        TextureRegion locked = new TextureRegion(
                TextureManager.getTilesetTexture(),
                Utils.TILE_WIDTH_INT * 10,
                Utils.TILE_HEIGHT_INT * 0,
                Utils.TILE_WIDTH_INT,
                Utils.TILE_HEIGHT_INT
        );
        TextureRegion unlocked = new TextureRegion(
                TextureManager.getTilesetTexture(),
                Utils.TILE_WIDTH_INT * 5,
                Utils.TILE_HEIGHT_INT * 0,
                Utils.TILE_WIDTH_INT,
                Utils.TILE_HEIGHT_INT
        );
        this.lockedTexture = locked;
        this.unlockedTexture = unlocked;
        isLocked = true;
        unlockKeyType = Utils.ItemType.KEY_1;

        setRegion(this.unlockedTexture);
    }

    public Door(TextureRegion locked, TextureRegion unlocked) {
        super();
        type = Utils.InteractiveType.DOOR;
        isCollider = true;

        this.lockedTexture = locked;
        this.unlockedTexture = unlocked;

        isLocked = false;
        unlockKeyType = Utils.ItemType.KEY_1;

        setRegion(this.unlockedTexture);
    }

    public boolean isLocked() {
        return isLocked;
    }

    public void setLocked(boolean locked) {
        isLocked = locked;
        setRegion(isLocked ? lockedTexture : unlockedTexture);
    }

    public void setOpen(boolean open) {
        if (open && !isLocked) {
            isVisible = false;
            isCollider = false;
        } else if(!open) {
            isVisible = true;
            isCollider = true;
        }
    }

    public boolean isOpen() {
        return !isVisible;
    }

    public Utils.ItemType getUnlockKeyType() {
        return unlockKeyType;
    }

    public void setUnlockKeyType(Utils.ItemType itemType) {
        this.unlockKeyType = itemType;
    }

    @Override
    public boolean onUseWith(InventoryItem item) {
        if(item.getType() == unlockKeyType) {
            setLocked(false);
            return true;
        }
        return false;
    }

    @Override
    public boolean onContact(WorldComponent component) {
        return false;
    }
}
