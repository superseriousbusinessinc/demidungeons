package com.ssbi.demidungeons.objects.interactive;

import com.ssbi.demidungeons.objects.InventoryItem;
import com.ssbi.demidungeons.objects.WorldComponent;
import com.ssbi.demidungeons.utils.Utils;

/**
 * Created by Benjamin on 2015-10-25.
 */
public abstract class Interactive extends WorldComponent {
    protected Utils.InteractiveType type;
    protected boolean isCollider;
    public Interactive() {
        super();
        isCollider = true;
    }

    public boolean isCollider() {
        return isCollider;
    }

    public abstract boolean onUseWith(InventoryItem item);
    public abstract boolean onContact(WorldComponent component);
}
