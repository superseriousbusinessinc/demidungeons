package com.ssbi.demidungeons.objects;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.ssbi.demidungeons.utils.*;

/**
 * Created by Benjamin on 2015-10-24.
 */
public class InventoryItem extends WorldComponent {
    protected Utils.ItemType type;

    public InventoryItem(Utils.ItemType itemType) {
        super();
        type = itemType;
        TextureRegion tr = com.ssbi.demidungeons.utils.TextureManager.getItemTexture(itemType);
        setRegion(tr);
        setSize(Utils.TILE_WIDTH, Utils.TILE_HEIGHT);
    }

    public Utils.ItemType getType() {
        return type;
    }
}
