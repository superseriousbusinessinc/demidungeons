package com.ssbi.demidungeons.objects;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.ssbi.demidungeons.utils.Utils;

/**
 * Created by Benjamin on 2015-10-20.
 */
public class WorldComponent extends Sprite {
    protected boolean isFacingRight;
    protected boolean isVisible;
    protected boolean isCollider;

    protected Vector2 gridPos;

    public WorldComponent() {
        super();
        isFacingRight = true;
        isVisible = true;
        isCollider = false;
        gridPos = new Vector2();
    }

    public WorldComponent(TextureRegion textureRegion) {
        super(textureRegion);
        isFacingRight = true;
        isVisible = true;
        isCollider = false;
        gridPos = new Vector2();
    }

    @Override
    public void draw(Batch batch) {
        if(isVisible)
            super.draw(batch);
    }

    public boolean isFacingRight() {
        return isFacingRight;
    }

    public void setFacingRight(boolean isFacingRight) {
        this.isFacingRight = isFacingRight;
    }

    public boolean isVisible() {
        return isVisible;
    }

    public void setVisible(boolean isVisible) {
        this.isVisible = isVisible;
    }

    public boolean isCollider() {
        return isCollider;
    }

    public void setCollider(boolean isColider) {
        this.isCollider = isCollider;
    }

    @Override
    public void setPosition(float x, float y) {
        super.setPosition(x, y);
        gridPos.set((int)(x + (getWidth() / 2.0f)) - (int)(x + (getWidth() / 2.0f)) % Utils.TILE_WIDTH,
                    (int)(y + (getHeight() / 2.0f)) - (int)(y + (getHeight() / 2.0f)) % Utils.TILE_HEIGHT);
    }

    public void setGridPosition(int gridX, int gridY) {
        setPosition(gridX * Utils.TILE_WIDTH, gridY * Utils.TILE_HEIGHT);
    }

    public Vector2 getGridPosition() {
        return gridPos;
    }

    public int getGridX() {
        return (int)gridPos.x;
    }

    public int getGridY() {
        return (int)gridPos.y;
    }

    public void snapToGrid() {
        super.setPosition(gridPos.x, gridPos.y);
    }
}
