package com.ssbi.demidungeons.objects.characters;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.math.Vector2;
import com.ssbi.demidungeons.levels.GameLevel;
import com.ssbi.demidungeons.objects.Avatar;
import com.ssbi.demidungeons.utils.Utils;

/**
 * Created by Benjamin on 2015-10-20.
 */
public class Player extends Avatar {

    public Player(GameLevel level, Utils.AvatarType playerType) {
        super(level);
        type = playerType;

        TextureRegion playerTexture = null;
        switch (type) {
            case P_WARRIOR:
                playerTexture = new TextureRegion(new Texture(Gdx.files.internal("images/warrior.png")));
                break;
            case P_MAGE:
                playerTexture = new TextureRegion(new Texture(Gdx.files.internal("images/mage.png")));
                break;
            case P_ROGUE:
                playerTexture = new TextureRegion(new Texture(Gdx.files.internal("images/rogue.png")));
                break;
            case P_HUNTER:
                playerTexture = new TextureRegion(new Texture(Gdx.files.internal("images/ranger.png")));
                break;
        }

        int trW = 12;
        int trH = 15;
        int trX = 0;
        int trY = trH * 6;
        TextureRegion[] idleFrames = new TextureRegion[5]; // Length set to 5 for HACKY COOL IDLE
        TextureRegion[] walkFrames = new TextureRegion[6];
        TextureRegion[] dieFrames = new TextureRegion[5];
        // Get idle animation.
        for (int col = 0; col < idleFrames.length; col++) {
            idleFrames[col] = new TextureRegion(playerTexture, trX, trY, trW, trH);
            // HACK TO GET COOLER IDLE ANIMATIONS!
            if (col >= idleFrames.length - 2)
                trX += trW;
        }
        idleAnimation = new Animation(1.0f, idleFrames);
        // Get walk animation.
        for (int col = 0; col < walkFrames.length; col++) {
            walkFrames[col] = new TextureRegion(playerTexture, trX, trY, trW, trH);
            trX += trW;
        }
        walkAnimation = new Animation(0.1f, walkFrames);
        // Get die animation.
        for (int col = 0; col < dieFrames.length; col++) {
            dieFrames[col] = new TextureRegion(playerTexture, trX, trY, trW, trH);
            trX += trW;
        }
        dieAnimation = new Animation(0.2f, dieFrames);

        // Set initial player state.
        state = State.IDLE;
        setSize(Utils.TILE_WIDTH, Utils.TILE_HEIGHT);

        health = 100;
//        turnPoints = 3;
    }

    public void update(float delta) {
        super.update(delta);

        switch (state) {
            case IDLE:
                currentFrame = idleAnimation.getKeyFrame(stateTime, true);
                break;
            case WALK:
                float oldX = getX();
                move(delta);
                if (getX() > oldX)
                    isFacingRight = true;
                else if (getX() < oldX)
                    isFacingRight = false;
                currentFrame = walkAnimation.getKeyFrame(stateTime, true);
                break;
            case DEAD:
                currentFrame = dieAnimation.getKeyFrame(stateTime, false);
                break;
        }
        if ((!isFacingRight && !currentFrame.isFlipX()) || (isFacingRight && currentFrame.isFlipX()))
            currentFrame.flip(true, false);
        setRegion(currentFrame);
    }

    public void setWalkDestination(Vector2 destination, TiledMapTileLayer mLayer) {
        super.setWalkDestination(destination, mLayer);
    }

    // Private methods
    private void move(float delta) {
        float oldX = getX(), oldY = getY();
        float newX = oldX, newY = oldY;

        setOriginalPos(oldX,oldY);

        if (walkDestination.x > oldX) {
            newX += speed * delta;
            if (newX > walkDestination.x)
                newX = walkDestination.x;
        } else if (walkDestination.x < oldX) {
            newX -= speed * delta;
            if (newX < walkDestination.x)
                newX = walkDestination.x;
        }
        if (walkDestination.y > oldY) {
            newY += speed * delta;
            if (newY > walkDestination.y)
                newY = walkDestination.y;
        } else if (walkDestination.y < oldY) {
            newY -= speed * delta;
            if (newY < walkDestination.y)
                newY = walkDestination.y;
        }

//        if (!isColliding(newX, newY))
            setPosition(newX, newY);
//
//        Vector2 dir = new Vector2(newX - originalX, newY - originalY);
//        if(dir.x >= 16 || dir.x <= -16 || dir.y >= 16 || dir.y <= -16)
//            setGridPos(rounder(newX), rounder(newY));

        if (getX() == walkDestination.x && getY() == walkDestination.y) {
//            setGridPos(rounder(newX), rounder(newY));
            state = State.IDLE;
            stateTime = 0;
            curTurnPoints--;
        }
    }

    @Override
    public void damaged(int healthDamaged) {
        health -= healthDamaged;
        showDamageEffect();
        if (health <= 0) {
            health = 0;
            state = Avatar.State.DEAD;
            stateTime = 0;
            level.dropInventory(this);
        }
    }

    @Override
    public void doTurn(GameLevel level) {

    }

}
