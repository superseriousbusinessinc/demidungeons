package com.ssbi.demidungeons.objects.characters;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.math.Vector2;
import com.ssbi.demidungeons.levels.GameLevel;
import com.ssbi.demidungeons.objects.Avatar;
import com.ssbi.demidungeons.utils.Utils;

/**
 * Created by Benjamin on 2015-10-24.
 */
public class Rat extends Avatar {

    private static Texture texture = null;

    public Rat(GameLevel level) {
        super(level);
        type = Utils.AvatarType.E_RAT;
        if (texture == null)
            texture = new Texture(Gdx.files.internal("images/rat.png"));

        int trW = 16;
        int trH = 15;
        int trX = 0;
        int trY = 0;
        TextureRegion[] idleFrames = new TextureRegion[2];
        TextureRegion[] walkFrames = new TextureRegion[6];
        TextureRegion[] dieFrames = new TextureRegion[4];
        // Get idle animation.
        for (int col = 0; col < idleFrames.length; col++) {
            idleFrames[col] = new TextureRegion(texture, trX, trY, trW, trH);
            trX += trW;
        }
        idleAnimation = new Animation(5.0f, idleFrames);
        // Get walk animation.
        trX = trW * 5;
        for (int col = 0; col < walkFrames.length; col++) {
            walkFrames[col] = new TextureRegion(texture, trX, trY, trW, trH);
            trX += trW;
        }
        walkAnimation = new Animation(0.1f, walkFrames);
        // Get die animation.
        for (int col = 0; col < dieFrames.length; col++) {
            dieFrames[col] = new TextureRegion(texture, trX, trY, trW, trH);
            trX += trW;
        }
        dieAnimation = new Animation(0.2f, dieFrames);

        // Set initial player state.
        state = State.IDLE;
        setSize(Utils.TILE_WIDTH, Utils.TILE_HEIGHT);

        stateTime = 0.0f;

        health = 20;
//        turnPoints = 2;
    }

    public void update(float delta) {
        super.update(delta);

        switch (state) {
            case IDLE:
                currentFrame = walkAnimation.getKeyFrame(0);
                break;
            case WALK:
                float oldX = getX();
                move(delta);
                if (getX() > oldX)
                    isFacingRight = true;
                else if (getX() < oldX)
                    isFacingRight = false;
                currentFrame = walkAnimation.getKeyFrame(stateTime, true);
                break;
            case DEAD:
                currentFrame = dieAnimation.getKeyFrame(stateTime, false);
                break;
        }
        if ((!isFacingRight && !currentFrame.isFlipX()) || (isFacingRight && currentFrame.isFlipX()))
            currentFrame.flip(true, false);
        setRegion(currentFrame);
    }


    public void setWalkDestination(Vector2 destination, TiledMapTileLayer mLayer) {
        super.setWalkDestination(destination, mLayer);
    }


    // Private methods
    private void move(float delta) {
        float oldX = getX(), oldY = getY();
        float newX = oldX, newY = oldY;

        setOriginalPos(oldX,oldY);

        if (walkDestination.x > oldX) {
            newX += speed * delta;
            if (newX > walkDestination.x)
                newX = walkDestination.x;
        } else if (walkDestination.x < oldX) {
            newX -= speed * delta;
            if (newX < walkDestination.x)
                newX = walkDestination.x;
        }
        if (walkDestination.y > oldY) {
            newY += speed * delta;
            if (newY > walkDestination.y)
                newY = walkDestination.y;
        } else if (walkDestination.y < oldY) {
            newY -= speed * delta;
            if (newY < walkDestination.y)
                newY = walkDestination.y;
        }

//        if (!isColliding(newX, newY))
            setPosition(newX, newY);
//
//        Vector2 dir = new Vector2(newX - originalX, newY - originalY);
//        if(dir.x >= 16 || dir.x <= -16 || dir.y >= 16 || dir.y <= -16)
//            setGridPos(rounder(newX), rounder(newY));


        if (getX() == walkDestination.x && getY() == walkDestination.y) {
//            setGridPos(rounder(newX), rounder(newY));
            state = State.IDLE;
            stateTime = 0;
            curTurnPoints--;
        }

    }

    @Override
    public void damaged(int healthDamaged) {
        if(state == State.DEAD) {
            return;
        }
        health -= healthDamaged;
        showDamageEffect();
        if (health <= 0) {
            health = 0;
            state = Avatar.State.DEAD;
            isCollider = false;
            stateTime = 0;
            level.dropInventory(this);
        }
    }

    @Override
    public void doTurn(GameLevel level) {
        if(state == State.DEAD) {
            endTurn();
        }

        if(state != State.IDLE) {
            return;
        }

        Avatar p = level.getPlayer();
        // Walk collision check. (Rat moves towards the player)
        Vector2 dir = new Vector2(p.getX() - getX(), p.getY() - getY());
        Vector2 walkDest = new Vector2(getX(), getY());
        if((dir.x == 16.0 || dir.x == 0 || dir.x == -16.0)&&(dir.y == 16.0 || dir.y == 0 || dir.y == -16.0))
        {
            p.damaged(10);
            curTurnPoints--;
        }
        else {
            if (dir.x > 0)
                walkDest.x += Utils.TILE_WIDTH;
            else if (dir.x < 0)
                walkDest.x -= Utils.TILE_WIDTH;
            if (dir.y > 0)
                walkDest.y += Utils.TILE_HEIGHT;
            else if (dir.y < 0)
                walkDest.y -= Utils.TILE_HEIGHT;
            if (!level.hasCollision(walkDest.x, walkDest.y)) {
                setWalkDestination(walkDest, level.getMainLayer());
            } else {
                curTurnPoints--;
            }
        }
    }
}
