package com.ssbi.demidungeons.tween;

import com.badlogic.gdx.graphics.g2d.Sprite;

import aurelienribon.tweenengine.TweenAccessor;

/**
 * Created by rajib on 2015-09-25.
 */
public class SpriteAccessor implements TweenAccessor<Sprite> {
    public static final int ALPHA = 0;
    @Override
    public int getValues(Sprite target, int tweenType, float[] returnValues) {
        switch(tweenType){
            case ALPHA:
                returnValues[0] = target.getColor().a;
            default:
                assert false;
                return -1;

        }
    }

    @Override
    public void setValues(Sprite target, int tweenType, float[] newValues) {
   switch(tweenType){
            case ALPHA:
                target.setColor(target.getColor().r, target.getColor().g, target.getColor().b, newValues[0]);
                break;
       default:
           assert false;
        }
    }
}
