package com.ssbi.demidungeons;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.ssbi.demidungeons.ui.screens.GameScreen;
import com.ssbi.demidungeons.ui.screens.MainMenu;
import com.ssbi.demidungeons.ui.screens.SplashScreen;
import com.ssbi.demidungeons.utils.DemiDungeonsConfig;
import com.ssbi.demidungeons.utils.EmailManager;
import com.ssbi.demidungeons.utils.Utils;

public class DemiDungeonsGame extends Game {
    public static boolean devmode = false;
    public static DemiDungeonsGame game;

    private enum StartPoint {
        SPLASH,
        MAIN,
        GAME
    }

    private StartPoint start = StartPoint.SPLASH;

    private EmailManager emailManager;

    public DemiDungeonsGame(DemiDungeonsConfig config) {
        emailManager = config.emailManager;
    }

    @Override
    public void create() {
        game = this;
        Preferences pref = Gdx.app.getPreferences(Utils.PREF_LEVEL_STATE);
        int curScreen =  pref.getInteger(Utils.VAL_CURRENT_SCREEN, Utils.SCREEN_MAIN);
        if(curScreen == Utils.SCREEN_GAME) {
            start = StartPoint.GAME;
        }

        switch (start) {
            case SPLASH:
                setScreen(new SplashScreen());
                break;
            case MAIN:
                setScreen(new MainMenu());
                break;
            case GAME:
                int curLevel =  pref.getInteger(Utils.VAL_CURRENT_LEVEL, 1);
                setScreen(new GameScreen(curLevel,true));
                break;
        }
    }

    public EmailManager getEmailManager() {
        return emailManager;
    }


    @Override
    public void dispose() {
        Gdx.app.exit();
    }
}
