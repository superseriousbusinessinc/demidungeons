package com.ssbi.demidungeons.ui.hud;

import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.ssbi.demidungeons.DemiDungeonsGame;
import com.ssbi.demidungeons.levels.GameLevel;
import com.ssbi.demidungeons.ui.screens.GameScreen;
import com.ssbi.demidungeons.ui.screens.MainMenu;
import com.ssbi.demidungeons.utils.TextureManager;
import com.ssbi.demidungeons.utils.Utils;

/**
 * Created by Benjamin on 2015-11-28.
 */
public class GameMenu extends Stage {

    private GameHUD hud;

    private Table tblMain;

    private Button btnResume;
    private Button btnRestart;
    private Button btnMainMenu;

    private BitmapFont font;

    public GameMenu(GameHUD hud) {
        super();
        this.hud = hud;

        font = new BitmapFont();

        Pixmap tblBG = new Pixmap(this.hud.screenWidth, this.hud.screenHeight, Pixmap.Format.RGBA8888);
        tblBG.setColor(Color.BLACK);
        tblBG.fill();
        SpriteDrawable bg = new TextureRegionDrawable(new TextureRegion(new Texture(tblBG))).tint(new Color(0.0f, 0.0f, 0.0f, 0.8f));

        SpriteDrawable drwBtnBrownU = new TextureRegionDrawable(TextureManager.getButtonsTexture(Utils.ButtonsType.BROWN)).tint(new Color(1.0f, 1.0f, 1.0f, 0.9f));
        drwBtnBrownU.setMinWidth(hud.screenWidth / 4);
        drwBtnBrownU.setMinHeight(hud.screenHeight / 10);
        SpriteDrawable drwBtnBrownD = drwBtnBrownU.tint(Color.LIGHT_GRAY);

        TextButton.TextButtonStyle txtBtnStyle = new TextButton.TextButtonStyle();
        txtBtnStyle.up = drwBtnBrownU;
        txtBtnStyle.down = drwBtnBrownD;
        txtBtnStyle.font = font;

        btnResume = new TextButton("RESUME", txtBtnStyle);
        btnRestart = new TextButton("RESTART", txtBtnStyle);
        btnMainMenu = new TextButton("MAIN MENU", txtBtnStyle);

        tblMain = new Table();
        tblMain.background(bg);
        tblMain.center();
        tblMain.setFillParent(true);
        tblMain.add(btnResume);
        tblMain.row().pad(5);
        tblMain.add(btnRestart);
        tblMain.row();
        tblMain.add(btnMainMenu);

        this.addActor(tblMain);

        ClickListener btnListener = new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (event.getListenerActor() == btnResume) {
                    setVisible(false);
                } else if (event.getListenerActor() == btnRestart) {
                    Screen curScreen = DemiDungeonsGame.game.getScreen();
                    if (curScreen instanceof GameScreen) {
                        ((GameScreen) curScreen).restart();
                    }
                } else if (event.getListenerActor() == btnMainMenu) {
                    DemiDungeonsGame.game.setScreen(new MainMenu());
                }
            }
        };
        btnResume.addListener(btnListener);
        btnRestart.addListener(btnListener);
        btnMainMenu.addListener(btnListener);
    }

    public void resize(int width, int height) {
        if (font != null) {
            font.getData().setScale(width / 800.0f * 1.5f);
        }
    }

    public void setVisible(boolean visible) {
        tblMain.setVisible(visible);
    }

    public boolean isVisible() {
        return tblMain.isVisible();
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        if (!isVisible()) {
            return false;
        }
        // If touching menu area
        float t = btnResume.getY() + btnResume.getHeight();
        float r = btnResume.getX() + btnResume.getWidth();
        float b = btnMainMenu.getY();
        float l = btnMainMenu.getX();
        if (screenX > l && screenX < r && screenY < t && screenY > b) {
            return super.touchUp(screenX, screenY, pointer, button);
        } else {
            setVisible(false);
        }
        return true;
    }
}
