package com.ssbi.demidungeons.ui.hud;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.ssbi.demidungeons.objects.Avatar;
import com.ssbi.demidungeons.objects.InventoryItem;
import com.ssbi.demidungeons.utils.TextureManager;
import com.ssbi.demidungeons.utils.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Benjamin on 2015-11-28.
 */
public class InventoryDisplay extends Stage {

    private GameHUD hud;
    private List<InventoryItem> inv;

    private Table tblInv;
    private List<ImageButton> invBtns;

    public InventoryDisplay(GameHUD gameHUD) {
        hud = gameHUD;
        inv = hud.getLevel().getPlayer().getInventory();

        TextureRegionDrawable txInvU = new TextureRegionDrawable(TextureManager.getToolbarTexture(Utils.ToolbarType.INV_SLOT));
        txInvU.setMinWidth(hud.screenWidth / 10);
        txInvU.setMinHeight(hud.screenWidth / 10);
        Drawable txInvD = txInvU.tint(Color.LIGHT_GRAY);
        txInvD.setMinWidth(hud.screenWidth / 10);
        txInvD.setMinHeight(hud.screenWidth / 10);

        invBtns = new ArrayList<ImageButton>(Utils.INV_SLOTS);
        tblInv = new Table();
        tblInv.pad(10).right();
        tblInv.setFillParent(true);
        for (int i = 0; i < Utils.INV_SLOTS; i++) {
            final int j = i;
            ImageButton.ImageButtonStyle btnStyleInv = new ImageButton.ImageButtonStyle();
            btnStyleInv.up = txInvU;
            btnStyleInv.down = txInvD;
            ImageButton newBtn = new ImageButton(btnStyleInv);
            invBtns.add(i, newBtn);
            if (i % 3 == 0)
                tblInv.row().pad(0);
            tblInv.add(invBtns.get(i));
            newBtn.addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    if (hud.getLevel().getCurrentTurn() != hud.getLevel().getPlayer() || hud.getLevel().getPlayer().getState() != Avatar.State.IDLE || hud.getLevel().getPlayer().isTurnFinished()) {
                        return;
                    }
                    if (j >= inv.size()) {
                        return;
                    }
                    InventoryItem invItem = inv.get(j);
                    switch (invItem.getType()) {
                        case KEY_0:
                            break;
                        case KEY_1:
                            break;
                        case POTION_2:
                            hud.getLevel().getPlayer().setHealth(100);
                            hud.getLevel().getCurrentTurn().endTurn();
                            hud.getLevel().getPlayer().removeInventoryItem(inv.get(j));
                            break;
                    }
                }
            });
        }

        this.addActor(tblInv);
    }

    public void update(float delta) {
        // Only update if visible
        if (!isVisible()) {
            return;
        }
        for (int i = 0; i < invBtns.size(); i++) {
            if (i >= inv.size()) {
                invBtns.get(i).getStyle().imageUp = null;
                invBtns.get(i).getStyle().imageDown = null;
                break;
            }
            InventoryItem itm = inv.get(i);

            TextureRegion tr = TextureManager.getItemTexture(itm.getType());
            TextureRegionDrawable txInvU = new TextureRegionDrawable(tr);
            txInvU.setMinWidth(hud.screenWidth / 12);
            txInvU.setMinHeight(hud.screenWidth / 12);
            Drawable txInvD = txInvU.tint(Color.LIGHT_GRAY);
            txInvD.setMinWidth(hud.screenWidth / 12);
            txInvD.setMinHeight(hud.screenWidth / 12);

            invBtns.get(i).getStyle().imageUp = txInvU;
            invBtns.get(i).getStyle().imageDown = txInvD;
        }
    }

    public void resize(int width, int height) {

    }

    public void setVisible(boolean visible) {
        tblInv.setVisible(visible);
    }

    public boolean isVisible() {
        return tblInv.isVisible();
    }
}
