package com.ssbi.demidungeons.ui.hud;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.ssbi.demidungeons.DemiDungeonsGame;
import com.ssbi.demidungeons.levels.GameLevel;
import com.ssbi.demidungeons.objects.Avatar;
import com.ssbi.demidungeons.ui.screens.InstructionsScreen;
import com.ssbi.demidungeons.utils.TextureManager;
import com.ssbi.demidungeons.utils.Utils;

/**
 * Created by Benjamin on 2015-10-25.
 */
public class GameHUD {

    private Avatar player;
    private GameLevel level;

    int screenWidth;
    int screenHeight;

    private Stage stage;

    private BitmapFont fontHealth;
    private BitmapFont fontBtnMenu;
    private BitmapFont fontCenter;

    private Label lblHealth;
    private Label lblCenter;
    private Button btnMenu;
    private Button btnInv;

    private GameMenu menu;
    private InventoryDisplay invDisplay;

    private InputMultiplexer inputMultiplexer;

    public GameHUD(GameLevel level) {
        this.player = level.getPlayer();
        this.level = level;

        screenWidth = Gdx.graphics.getWidth();
        screenHeight = Gdx.graphics.getHeight();

        stage = new Stage();

        fontHealth = new BitmapFont();
        fontBtnMenu = new BitmapFont();
        fontCenter = new BitmapFont();


        TextureRegionDrawable drwBtnRedU = new TextureRegionDrawable(TextureManager.getButtonsTexture(Utils.ButtonsType.RED));
        drwBtnRedU.setMinWidth(screenWidth / 12);
        drwBtnRedU.setMinHeight(screenHeight / 12);
        SpriteDrawable drwBtnRedD = drwBtnRedU.tint(Color.LIGHT_GRAY);

        TextButton.TextButtonStyle btnStyleMenu = new TextButton.TextButtonStyle();
        btnStyleMenu.up = drwBtnRedU;
        btnStyleMenu.down = drwBtnRedD;
        btnStyleMenu.font = fontBtnMenu;

        TextureRegionDrawable txInvU = new TextureRegionDrawable(TextureManager.getToolbarTexture(Utils.ToolbarType.INVENTORY));
        txInvU.setMinWidth(screenWidth / 10);
        txInvU.setMinHeight(screenWidth / 10);
        SpriteDrawable txInvD = txInvU.tint(Color.LIGHT_GRAY);
        txInvD.setMinWidth(screenWidth / 10);
        txInvD.setMinHeight(screenWidth / 10);
        ImageButton.ImageButtonStyle btnStyleInv = new ImageButton.ImageButtonStyle();
        btnStyleInv.imageUp = txInvU;
        btnStyleInv.imageDown = txInvD;

        lblHealth = new Label("HP: " + String.format("%03d", player.getHealth()), new Label.LabelStyle(fontHealth, Color.RED));
        btnMenu = new TextButton("MENU", btnStyleMenu);
        btnInv = new ImageButton(btnStyleInv);
        lblCenter = new Label("", new Label.LabelStyle(fontCenter, Color.BLUE));

        Table tblHealth = new Table();
        tblHealth.top().left();
        tblHealth.setFillParent(true);
        tblHealth.add(lblHealth).pad(10).fillX();

        Table tblMenu = new Table();
        tblMenu.top().right();
        tblMenu.setFillParent(true);
        tblMenu.add(btnMenu).pad(10).fillX();

        Table tblToolbar = new Table();
        tblToolbar.bottom().right();
        tblToolbar.setFillParent(true);
        tblToolbar.add(btnInv).pad(10).bottom().right();

        Table tblCenter = new Table();
        tblCenter.center();
        tblCenter.setFillParent(true);
        tblCenter.add(lblCenter).pad(10).fillX();

        stage.addActor(tblHealth);
        stage.addActor(tblMenu);
        stage.addActor(tblToolbar);
        stage.addActor(tblCenter);

        // Set click listeners for buttons
        ClickListener btnClickListener = new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if(event.getListenerActor() == btnMenu) {
                    toggleMenu();
                } else if(event.getListenerActor() == btnInv) {
                    toggleInventory();
                }
            }
        };
        btnMenu.addListener(btnClickListener);
        btnInv.addListener(btnClickListener);

        menu = new GameMenu(this);
        menu.setVisible(false);

        invDisplay = new InventoryDisplay(this);
        invDisplay.setVisible(false);

        inputMultiplexer = new InputMultiplexer();
        inputMultiplexer.addProcessor(menu);
        inputMultiplexer.addProcessor(invDisplay);
        inputMultiplexer.addProcessor(stage);
    }

    public void update(float delta) {
        invDisplay.update(delta);

        lblHealth.setText("HP: " + player.getHealth());
        if (level.isGameFinished()) {
            invDisplay.setVisible(false);
            lblCenter.setText("Level " + level.getLevelNumber() + " Complete!");
        }
    }

    public void draw(Batch batch) {
        stage.draw();
        invDisplay.draw();
        menu.draw();
    }

    public void resize(int width, int height) {
        screenWidth = width;
        screenHeight = height;

        float scaleFontHealth = screenWidth / 800.0f * 2.5f;
        fontHealth.getData().setScale(scaleFontHealth);
        float scaleFontMenu = screenWidth / 800.0f * 1.25f;
        fontBtnMenu.getData().setScale(scaleFontMenu);
        float scaleFontCenter = screenWidth / 800.0f * 3.5f;
        fontCenter.getData().setScale(scaleFontCenter);

        menu.resize(screenWidth, screenHeight);
    }

    public InputMultiplexer getInputMultiplexer() {
        return inputMultiplexer;
    }

    public GameLevel getLevel() {
        return level;
    }

    private void toggleMenu() {
        Utils.log("toggleMenu");
        menu.setVisible(!menu.isVisible());
    }

    private void toggleInventory() {
        Utils.log("toggleInventory");
        invDisplay.setVisible(!invDisplay.isVisible());
    }
}
