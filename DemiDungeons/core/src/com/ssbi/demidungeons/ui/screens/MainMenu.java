package com.ssbi.demidungeons.ui.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.ssbi.demidungeons.DemiDungeonsGame;
import com.ssbi.demidungeons.utils.TextureManager;
import com.ssbi.demidungeons.utils.Utils;

/**
 * Created by alpha_000 on 2015-09-25.
 */
public class MainMenu implements Screen {

    private int screenWidth;
    private int screenHeight;

    private Batch batch;
    private Texture txLogo;
    private Texture txTitle;
    private Stage stage;
    private TextButton btnPlay;
    private TextButton btnInstructions;
    private TextButton btnAbout;
    private BitmapFont font;
    private Table tblMain;

    public MainMenu() {
    }

    @Override
    public void show() {
        screenWidth = Gdx.graphics.getWidth();
        screenHeight = Gdx.graphics.getHeight();

        batch = new SpriteBatch();
        txLogo = new Texture(Gdx.files.internal("images/ddlogo.png"));
        txTitle = new Texture(Gdx.files.internal("images/ddtitle.png"));

        font = new BitmapFont();

        SpriteDrawable drwBtnBrownU = new TextureRegionDrawable(TextureManager.getButtonsTexture(Utils.ButtonsType.BROWN)).tint(new Color(1.0f, 1.0f, 1.0f, 0.9f));
        drwBtnBrownU.setMinWidth(screenWidth / 4);
        drwBtnBrownU.setMinHeight(screenHeight / 10);
        SpriteDrawable drwBtnBrownD = drwBtnBrownU.tint(Color.LIGHT_GRAY);

        TextButton.TextButtonStyle txtBtnStyle = new TextButton.TextButtonStyle();
        txtBtnStyle.up = drwBtnBrownU;
        txtBtnStyle.down = drwBtnBrownD;
        txtBtnStyle.font = font;

        btnPlay = new TextButton("PLAY", txtBtnStyle);
        btnInstructions = new TextButton("INSTRUCTIONS", txtBtnStyle);
        btnAbout = new TextButton("ABOUT", txtBtnStyle);

        TextureRegionDrawable drwTitle = new TextureRegionDrawable(new TextureRegion(txTitle));
        float titleW = screenWidth / 1.5f;
        float titleH = titleW * ((float) txTitle.getHeight() / (float) txTitle.getWidth());
        drwTitle.setMinWidth(titleW);
        drwTitle.setMinHeight(titleH);
        Drawable drwLogo = new TextureRegionDrawable(new TextureRegion(txLogo));

        Table tblTitle = new Table();
        tblTitle.background(drwTitle);

        tblMain = new Table();
        tblMain.background(drwLogo);
        tblMain.center();
        tblMain.setFillParent(true);
        tblMain.add(tblTitle).fill();
        tblMain.row().pad(0.0f, 0.0f, 50.0f, 0.0f);
        tblMain.add();
        tblMain.row();
        tblMain.add(btnPlay);
        tblMain.row().pad(5.0f);
        tblMain.add(btnInstructions);
        tblMain.row();
        tblMain.add(btnAbout);

        stage = new Stage();
        stage.addActor(tblMain);


        btnInstructions.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                DemiDungeonsGame.game.setScreen(new InstructionsScreen());
            }
        });

        btnAbout.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                DemiDungeonsGame.game.setScreen(new AboutScreen());
            }
        });

        btnPlay.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                DemiDungeonsGame.game.setScreen(new LevelSelectorScreen());
            }
        });
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0.5f, 0.5f, 0.5f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        batch.begin();
//        batch.draw(txLogo, 25, 0, 600, 400);
//        batch.draw(txTitle, 100, 300, 450, 150);
        batch.end();

        stage.draw();
    }

    @Override
    public void hide() {
    }

    @Override
    public void pause() {
        Preferences pref = Gdx.app.getPreferences(Utils.PREF_LEVEL_STATE);
        pref.putInteger(Utils.VAL_CURRENT_SCREEN, Utils.SCREEN_MAIN);
        pref.flush();
    }

    @Override
    public void resume() {
    }

    @Override
    public void resize(int width, int height) {
        screenWidth = width;
        screenHeight = height;
        if (font != null) {
            font.getData().setScale(screenWidth / 800.0f * 1.5f);
        }
        stage.getViewport().setScreenBounds(0, 0, screenWidth, screenHeight);
    }

    @Override
    public void dispose() {
        txLogo.dispose();
        txTitle.dispose();
        stage.dispose();
        batch.dispose();
    }
}
