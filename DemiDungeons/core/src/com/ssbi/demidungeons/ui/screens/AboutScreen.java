package com.ssbi.demidungeons.ui.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.ssbi.demidungeons.DemiDungeonsGame;
import com.ssbi.demidungeons.utils.EmailManager;
import com.ssbi.demidungeons.utils.TextureManager;
import com.ssbi.demidungeons.utils.Utils;

/**
 * Created by rajib on 2015-10-01.
 */
public class AboutScreen implements Screen {
    int screenWidth;
    int screenHeight;

    SpriteBatch batch = new SpriteBatch();
    BitmapFont btnFont;
    BitmapFont lblFont;

    TextButton btnMainMenu;
    TextButton btnContact;
    Label lblAbout;
    Stage stage;

    @Override
    public void show() {
        screenWidth = Gdx.graphics.getWidth();
        screenHeight = Gdx.graphics.getHeight();

        batch = new SpriteBatch();
        stage = new Stage();
        Gdx.input.setInputProcessor(stage);
        btnFont = new BitmapFont();
        lblFont = new BitmapFont();

        TextureRegionDrawable trBtnU = new TextureRegionDrawable(TextureManager.getButtonsTexture(Utils.ButtonsType.RED));
        trBtnU.setMinWidth(screenWidth / 6.5f);
        trBtnU.setMinHeight(screenHeight / 10);
        SpriteDrawable trBtnD = trBtnU.tint(Color.LIGHT_GRAY);
        trBtnD.setMinWidth(screenWidth / 7);
        trBtnD.setMinHeight(screenHeight / 10);
        TextButton.TextButtonStyle txtBtnStyle = new TextButton.TextButtonStyle();
        txtBtnStyle.up = trBtnU;
        txtBtnStyle.down = trBtnD;
        txtBtnStyle.font = btnFont;
        btnMainMenu = new TextButton("Main Menu", txtBtnStyle);

        trBtnU = new TextureRegionDrawable(TextureManager.getButtonsTexture(Utils.ButtonsType.GREEN));
        trBtnU.setMinWidth(screenWidth / 6.5f);
        trBtnU.setMinHeight(screenHeight / 10);
        trBtnD = trBtnU.tint(Color.LIGHT_GRAY);
        trBtnD.setMinWidth(screenWidth / 7);
        trBtnD.setMinHeight(screenHeight / 10);
        txtBtnStyle = new TextButton.TextButtonStyle();
        txtBtnStyle.up = trBtnU;
        txtBtnStyle.down = trBtnD;
        txtBtnStyle.font = btnFont;
        btnContact = new TextButton("Contact Us", txtBtnStyle);

        Label.LabelStyle lblStyle = new Label.LabelStyle();
        lblStyle.font = lblFont;

        String about = "About Us: " +
                "\n  We are Super Serious Business and we are super" +
                "\n  serious about our business." +
                "\n" +
                "\nThe Team:" +
                "\n Aaron Murphy" +
                "\n Benjamin Priyadamkol" +
                "\n Byron Roby" +
                "\n Rajib Saha";
        lblAbout = new Label(about, lblStyle);
        lblAbout.setWrap(true);
        Table tblMain = new Table();
        tblMain.left();
        tblMain.setFillParent(true);
        tblMain.add(lblAbout).pad(10);

        Table tblTR = new Table();
        tblTR.top().right();
        tblTR.setFillParent(true);
        tblTR.add(btnMainMenu).pad(10).fillX();

        Table tblBL = new Table();
        tblBL.bottom().left();
        tblBL.setFillParent(true);
        tblBL.add(btnContact).pad(10).fillX();

        stage.addActor(tblTR);
        stage.addActor(tblBL);
        stage.addActor(tblMain);

        btnMainMenu.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                DemiDungeonsGame.game.setScreen(new MainMenu());
            }
        });
        btnContact.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                Utils.log("CONTACT US");
                EmailManager emailManager = DemiDungeonsGame.game.getEmailManager();
                if(emailManager != null) {
                    emailManager.sendEmail(null, "superseriousbusiness@gmail.com", "Hey!", null);
                }
            }
        });
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0.5f, 0.5f, 0.5f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        batch.begin();
//        btnMainMenu.draw(batch, 1.0f);
        batch.end();


        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        screenWidth = width;
        screenHeight = height;
        btnFont.getData().setScale(width / 800.0f * 1.5f);
        lblFont.getData().setScale(width / 800.0f * 1.5f);
    }

    @Override
    public void pause() {
        Preferences pref = Gdx.app.getPreferences(Utils.PREF_LEVEL_STATE);
        pref.putInteger(Utils.VAL_CURRENT_SCREEN, 4);
        pref.flush();
    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        batch.dispose();
        btnFont.dispose();
        lblFont.dispose();
        stage.dispose();
    }
}
