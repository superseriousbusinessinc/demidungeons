package com.ssbi.demidungeons.ui.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.ssbi.demidungeons.DemiDungeonsGame;

/**
 * Created by Benjamin on 2015-10-23.
 */
public class LoadingScreen implements Screen {
    private double timePass = 0;
    private SpriteBatch batch;
    public static final double TIME_TO_FADE = 2.0;
    private float alpha = 1;
    BitmapFont font;

    public LoadingScreen() {
        batch = new SpriteBatch();
    }
    @Override
    public void show() {
        font = new BitmapFont();
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        // Code to slowly fade out logo.
        timePass += delta;
        if(timePass >=2) {
            alpha = 2 - (float) (timePass / TIME_TO_FADE);
            if (alpha < 0) alpha = 0;
        }
        if(alpha <= 0) {
            DemiDungeonsGame.game.setScreen(new GameScreen(1, false));
        }

        batch.begin();
        batch.setColor(1, 1, 1, alpha);
        batch.end();
        Gdx.gl.glClearColor(0.5f, 0.5f, 0.5f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        batch = new SpriteBatch();
        font = new BitmapFont(Gdx.files.internal("fonts/calibri.fnt"),Gdx.files.internal("fonts/calibri.png"),false);
        font.setColor(Color.BLACK);
        batch.begin();
        font.draw(batch, "Loading..."
                , 200, 230, 1, 10, true);
        batch.end();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        batch.dispose();
    }
}
