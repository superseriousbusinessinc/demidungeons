package com.ssbi.demidungeons.ui.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.ssbi.demidungeons.DemiDungeonsGame;
import com.ssbi.demidungeons.utils.TextureManager;
import com.ssbi.demidungeons.utils.Utils;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by rajib on 2015-11-27.
 */


public class LevelSelectorScreen implements Screen {
    int screenWidth;
    int screenHeight;

    private Table tblLvl;
    private List<TextButton> lvlBtns;
    TextButton btnMainMenu;
    private String displayLvlInstructions;
    private Label displayLblIns;


    Stage stage;
    BitmapFont lvlDisplayFont;
    BitmapFont lvlInstFont;
    BitmapFont btnMainFont;

    TextureRegionDrawable trBtnMainLvlU;
    SpriteDrawable trBtnMainLvlD;

    TextureRegionDrawable trBtnLvlU;
    SpriteDrawable trBtnLvlD;
    SpriteDrawable trBtnInvalidU;

    public LevelSelectorScreen() {
        screenWidth = Gdx.graphics.getWidth();
        screenHeight = Gdx.graphics.getHeight();
        //Main Menu Button
        TextButton.TextButtonStyle btnStyleMainMenu = new TextButton.TextButtonStyle();
        trBtnMainLvlU = new TextureRegionDrawable(TextureManager.getButtonsTexture(Utils.ButtonsType.RED));

        trBtnMainLvlU.setMinWidth(screenWidth / 6.5f);
        trBtnMainLvlU.setMinHeight(screenHeight / 10);
        trBtnMainLvlD = trBtnMainLvlU.tint(Color.LIGHT_GRAY);
        trBtnMainLvlD.setMinWidth(screenWidth / 7.0f);
        trBtnMainLvlD.setMinHeight(screenHeight / 10);
        btnMainFont = new BitmapFont();
        btnStyleMainMenu.up = trBtnMainLvlU;
        btnStyleMainMenu.down = trBtnMainLvlD;
        btnStyleMainMenu.font = btnMainFont;

        trBtnLvlU = new TextureRegionDrawable(TextureManager.getButtonsTexture(Utils.ButtonsType.GREEN));

        trBtnLvlU.setMinWidth(screenWidth / 7.0f);
        trBtnLvlU.setMinHeight(screenWidth / 7.0f);
        trBtnLvlD = trBtnLvlU.tint(Color.LIGHT_GRAY);
        trBtnLvlD.setMinWidth(screenWidth / 7.0f);
        trBtnLvlD.setMinHeight(screenWidth / 7.0f);
        lvlDisplayFont = new BitmapFont();
        lvlBtns = new ArrayList<TextButton>(Utils.HIGHEST_LEVEL);
        tblLvl = new Table();
        tblLvl.center();
        tblLvl.setFillParent(true);

        TextButton.TextButtonStyle btnStyleLvl = new TextButton.TextButtonStyle();
        btnStyleLvl.up = trBtnLvlU;
        btnStyleLvl.down = trBtnLvlD;
        btnStyleLvl.font = lvlDisplayFont;

        trBtnInvalidU = trBtnLvlU.tint(Color.PINK);
        trBtnInvalidU.setMinWidth(screenWidth / 7.0f);
        trBtnInvalidU.setMinHeight(screenWidth / 7.0f);
        TextButton.TextButtonStyle btnInvalidStyle = new TextButton.TextButtonStyle();
        btnInvalidStyle.up = trBtnInvalidU;
        btnInvalidStyle.font = lvlDisplayFont;

        Label.LabelStyle displayLblInsStyle = new Label.LabelStyle();
        lvlInstFont = new BitmapFont();
        displayLblInsStyle.font = lvlInstFont;
        displayLvlInstructions = "Select a level:";
        displayLblIns = new Label(displayLvlInstructions, displayLblInsStyle);
        tblLvl.add(displayLblIns).colspan(5);
        tblLvl.row();
        for (int i = 0; i < Utils.HIGHEST_LEVEL; i++) {
            final int j = i + 1;
            TextButton newBtn = null;
            Preferences pref = Gdx.app.getPreferences(Utils.PREF_SAVE_FILE);
            int latestLevel = pref.getInteger(Utils.VAL_LATEST_LEVEL, 1);

            if (j <= latestLevel) {
                newBtn = new TextButton("" + j, btnStyleLvl);
                lvlBtns.add(i, newBtn);
                //check to see if the user has selected any level
                lvlBtns.get(i).addListener(new ClickListener() {
                    @Override
                    public void clicked(InputEvent event, float x, float y) {
                        DemiDungeonsGame.game.setScreen(new GameScreen(j, false));
                    }
                });
            } else {
                newBtn = new TextButton("" + j, btnInvalidStyle);
                lvlBtns.add(i, newBtn);
            }

            if (i % 5 == 0)
                tblLvl.row().pad(2);
            tblLvl.add(lvlBtns.get(i));
        }
        btnMainMenu = new TextButton("Main Menu", btnStyleMainMenu);
        stage = new Stage();

        stage.addActor(tblLvl);
        stage.addActor(btnMainMenu);
        Gdx.input.setInputProcessor(stage);
        btnMainMenu.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                DemiDungeonsGame.game.setScreen(new MainMenu());
            }
        });


    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0.5f, 0.5f, 0.5f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        screenWidth = width;
        screenHeight = height;
        lvlInstFont.getData().setScale(width / 800.0f * 1.5f);
        lvlDisplayFont.getData().setScale(width / 800.0f * 2.5f);
        btnMainFont.getData().setScale(width / 800.0f * 1.5f);
        btnMainMenu.setPosition(screenWidth - screenWidth / 6.5f - 10.0f, screenHeight - screenHeight / 10.0f - 10.0f);
    }

    @Override
    public void pause() {
        Preferences pref = Gdx.app.getPreferences(Utils.PREF_LEVEL_STATE);
        pref.putInteger(Utils.VAL_CURRENT_SCREEN, 2);
        pref.flush();
    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
    }
}
