package com.ssbi.demidungeons.ui.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.ssbi.demidungeons.DemiDungeonsGame;
import com.ssbi.demidungeons.utils.TextureManager;
import com.ssbi.demidungeons.utils.Utils;

/**
 * Created by rajib on 2015-10-01.
 */
public class InstructionsScreen implements Screen {
    int screenWidth;
    int screenHeight;

    SpriteBatch batch = new SpriteBatch();
    BitmapFont btnFont;
    BitmapFont lblFont;

    TextButton btnMainMenu;
    Label lblInstructons;
    Stage stage;

    @Override
    public void show() {

        screenWidth = Gdx.graphics.getWidth();
        screenHeight = Gdx.graphics.getHeight();

        batch = new SpriteBatch();
        stage = new Stage();
        Gdx.input.setInputProcessor(stage);
        btnFont = new BitmapFont();
        lblFont = new BitmapFont();

        TextureRegionDrawable trBtnU = new TextureRegionDrawable(TextureManager.getButtonsTexture(Utils.ButtonsType.RED));
        trBtnU.setMinWidth(screenWidth / 6.5f);
        trBtnU.setMinHeight(screenHeight / 10);
        SpriteDrawable trBtnD = trBtnU.tint(Color.LIGHT_GRAY);
        trBtnD.setMinWidth(screenWidth / 7);
        trBtnD.setMinHeight(screenHeight / 10);
        TextButton.TextButtonStyle txtBtnStyle = new TextButton.TextButtonStyle();
        txtBtnStyle.up = trBtnU;
        txtBtnStyle.down = trBtnD;
        txtBtnStyle.font = btnFont;
        btnMainMenu = new TextButton("Main Menu", txtBtnStyle);

        Label.LabelStyle lblStyle = new Label.LabelStyle();
        lblStyle.font = lblFont;

        String instructions = "Game Instructions: " +
                "\n - To complete a level, unlock the cage door using the skeleton key" +
                "\n    and walk through." +
                "\n - To move your character select a space on the screen." +
                "\n - To open doors click on the door. If a door is locked, clicking" +
                "\n    on the door will unlock it if you have the correct key." +
                "\n - To attack an enemy click on the enemy." +
                "\n - Select an item in your inventory to use it." +
                "\n";
        lblInstructons = new Label(instructions, lblStyle);
        lblInstructons.setWrap(true);
        Table tblMain = new Table();
        tblMain.left();
        tblMain.setFillParent(true);
        tblMain.add(lblInstructons).pad(10);

        Table tblTR = new Table();
        tblTR.top().right();
        tblTR.setFillParent(true);
        tblTR.add(btnMainMenu).pad(10).fillX();

        stage.addActor(tblTR);
        stage.addActor(tblMain);

        btnMainMenu.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                DemiDungeonsGame.game.setScreen(new MainMenu());
            }
        });
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0.5f, 0.5f, 0.5f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        batch.begin();
//        btnMainMenu.draw(batch, 1.0f);
        batch.end();

        //lblAbout.setSize(screenWidth, screenHeight / 1.5f);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        screenWidth = width;
        screenHeight = height;
        btnFont.getData().setScale(width / 800.0f * 1.5f);
        lblFont.getData().setScale(width / 800.0f * 1.5f);
    }

    @Override
    public void pause() {
        Preferences pref = Gdx.app.getPreferences(Utils.PREF_LEVEL_STATE);
        pref.putInteger(Utils.VAL_CURRENT_SCREEN, 3);
        pref.flush();
    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        batch.dispose();
        btnFont.dispose();
        lblFont.dispose();
        stage.dispose();
    }
}
