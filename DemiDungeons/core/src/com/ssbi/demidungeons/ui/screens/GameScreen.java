package com.ssbi.demidungeons.ui.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.input.GestureDetector.GestureListener;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.ssbi.demidungeons.DemiDungeonsGame;
import com.ssbi.demidungeons.levels.GameLevel;
import com.ssbi.demidungeons.objects.Avatar;
import com.ssbi.demidungeons.objects.InventoryItem;
import com.ssbi.demidungeons.objects.interactive.Door;
import com.ssbi.demidungeons.objects.interactive.Interactive;
import com.ssbi.demidungeons.objects.interactive.StepTrap;
import com.ssbi.demidungeons.ui.hud.GameHUD;
import com.ssbi.demidungeons.utils.Utils;
import com.sun.org.apache.bcel.internal.generic.INSTANCEOF;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by rajib on 2015-10-01.
 */
public class GameScreen implements Screen, InputProcessor, GestureListener {
    private final float ZOOM_INC = 1.2f;

    private int screenWidth;
    private int screenHeight;
    private int initScreenWidth;
    private int initScreenHeight;

    private float initialZoom;

    private SpriteBatch hudBatch;

    private String levelGroup;
    private int levelNumber;
    private GameLevel level;
    private float timer;
    private GameHUD hud;
    // TODO: a little indicator to show which tile you have selected.
    private Sprite selection;

    private OrthogonalTiledMapRenderer renderer;
    private OrthographicCamera camera;
    private Vector3 touchDownPos;
    private boolean handledTouch;

    // debug stuff
    private static boolean debug = false;
    private BitmapFont fontFPS;
    private int fps;
    private int frameCount;
    private float fpsSecond;
    private Rectangle selectRect;
    boolean isLoadFromPref;

    public GameScreen(int levelNumber, boolean isLoadFromPref) {
        this.levelNumber = levelNumber;
        this.isLoadFromPref = isLoadFromPref;
    }

    @Override
    public void show() {
        try {
            levelGroup = "levels";
            level = new GameLevel(levelGroup, levelNumber);
            renderer = new OrthogonalTiledMapRenderer(level.getMap());
            camera = new OrthographicCamera();

            initScreenWidth = Gdx.graphics.getWidth();
            initScreenHeight = Gdx.graphics.getHeight();

            selection = new Sprite();

            hud = new GameHUD(level);

            InputMultiplexer multiplexer = new InputMultiplexer();
            multiplexer.addProcessor(hud.getInputMultiplexer());
            multiplexer.addProcessor(new GestureDetector(this));
            multiplexer.addProcessor(this);
            Gdx.input.setInputProcessor(multiplexer);

            hudBatch = new SpriteBatch();
            camera.position.set(level.getMapPixelWidth() / 2.0f, level.getMapPixelWidth() / 2.0f, 0);
            touchDownPos = new Vector3(camera.position);
            handledTouch = false;

            // Debug Stuff
            fontFPS = new BitmapFont();
            fps = 0;
            frameCount = 0;
            fpsSecond = 0.0f;
            selectRect = new Rectangle();
            selectRect.set(0.0f, 0.0f, Utils.TILE_WIDTH, Utils.TILE_HEIGHT);

            if (isLoadFromPref) {
                Preferences pref = Gdx.app.getPreferences(Utils.PREF_LEVEL_STATE);

                if (level.getRoundManager() != null)
                    level.getRoundManager().setCurTurn(pref.getInteger(Utils.VAL_CURRENT_TURN, level.getRoundManager().getCurTurn()));

                if (level.getPlayer() != null) {
                    level.getPlayer().setGridPos(pref.getInteger(Utils.VAL_PLAYER_X, 0), pref.getInteger(Utils.VAL_PLAYER_Y, (int) level.getPlayer().getY()));
                    level.getPlayer().setHealth(pref.getInteger(Utils.VAL_PLAYER_HEALTH, 0));
                    if (level.getPlayer().getHealth() <= 0) {
                        level.getPlayer().setState(Avatar.State.DEAD);
                    }
                }


                if (level.getEnemies() != null) {
                    level.setDeadEnemies(pref.getInteger(Utils.VAL_ALIVE_ENEMIES, 0));

                    int i = 0;
                    for (Avatar e : level.getEnemies()) {
                        e.setPosition(pref.getInteger("enemyPosX" + i, 0), pref.getInteger("enemyPosY" + i, 0));
                        e.setHealth(pref.getInteger("enemyHealth" + i, 0));
                        i++;
                    }
                }

                if (level.getDeadEnemies() != null) {
                    int i = 0;
                    for (Avatar d : level.getDeadEnemies()) {
                        d.setPosition(pref.getInteger("deadPosX" + i, 0), pref.getInteger("deadPosY" + i, 0));
                        i++;
                    }
                }

                int i = 0;
                for (Interactive ia : level.getInteractives()) {
                    if (ia instanceof Door) {
                        if (pref.getBoolean("isDoorLocked" + i, false) == false)
                            ((Door) ia).setLocked(false);
                        ((Door) ia).setOpen(pref.getBoolean("Door" + i, false));
                    } else if (ia instanceof StepTrap)
                        ((StepTrap) ia).setActive(pref.getBoolean("StepTrap" + i, false));
                    i++;
                }

                if (pref.getBoolean("cage_door", false)) {
                    level.getCageDoor().setLocked(false);
                    level.getCageDoor().setOpen(true);
                }

                int numOfPItems = pref.getInteger(Utils.VAL_PLAYER_INVENTORY_ITEM_NUM, 0);
                for (int c = 0; c < numOfPItems; c++) {
                    level.setInventoryItem(pref.getFloat("pItemX" + c, 0), pref.getFloat("pItemY" + c, 0));
                }

                List<InventoryItem> removeItems = new ArrayList<InventoryItem>();
                int numOfItems = pref.getInteger(Utils.VAL_ITEM_NUM, 0);

                for (InventoryItem item : level.getItems()) {
                    boolean found = false;
                    InventoryItem newItm = null;
                    for (int c = 0; c < numOfItems; c++) {
                        String strType = pref.getString("itemType" + c, "");
                        float itemX = pref.getFloat("itemX" + c, 0);
                        float itemY = pref.getFloat("itemY" + c, 0);
                        if (itemX == item.getX() && itemY == item.getY()) {
                            found = true;
                            break;
                        }
                        if (strType.equals("KEY_0")) {
                            newItm = new InventoryItem(Utils.ItemType.KEY_0);
                        } else if (strType.equals("KEY_1")) {
                            newItm = new InventoryItem(Utils.ItemType.KEY_1);
                        } else if (strType.equals("POTION_2")) {
                            newItm = new InventoryItem(Utils.ItemType.POTION_2);
                        }
                        if (newItm != null)
                            newItm.setPosition(itemX, itemY);
                    }
                    if (!found) {
                        removeItems.add(item);
                    }
                    if (newItm != null) {
                        level.getItems().add(newItm);
                    }
                }
                for (int c = 0; c < removeItems.size(); c++)
                    level.removeItem(removeItems.get(c));
            }
        } catch (Exception ex) {
            Utils.err(ex.getMessage(), ex);
        }
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0.5f, 0.5f, 0.5f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        if (level.isGameFinished()) {
            timer += delta;
            if (timer > 2.0f) {
                ++levelNumber;
                updateProgressData();
                if (levelNumber > Utils.HIGHEST_LEVEL) {
                    DemiDungeonsGame.game.setScreen(new MainMenu());
                } else {
                    DemiDungeonsGame.game.setScreen(new GameScreen(levelNumber, false));
                }
            }
        }

        level.update(delta);
        hud.update(delta);

        camera.position.set(
                level.getPlayer().getX() + (level.getPlayer().getWidth() / 2.0f),
                level.getPlayer().getY() + (level.getPlayer().getHeight() / 2.0f),
                0
        );
        camera.update();

        renderer.setView(camera);
        renderer.getBatch().begin();
        renderer.renderTileLayer(level.getMainLayer());
        level.draw(renderer.getBatch());
        renderer.getBatch().end();

        hudBatch.begin();
        hud.draw(hudBatch);
        hudBatch.end();

        if (debug) {
            ShapeRenderer sr = new ShapeRenderer();
            sr.setProjectionMatrix(camera.combined);
            sr.begin(ShapeRenderer.ShapeType.Line);
            // draw grid
            sr.setColor(1.0f, 1.0f, 0.0f, 1.0f);
            int w = level.getMainLayer().getWidth();
            int h = level.getMainLayer().getHeight();
            for (int i = 0; i < w; i++) {
                sr.line(0, i * Utils.TILE_WIDTH, h * Utils.TILE_HEIGHT, i * Utils.TILE_WIDTH);
            }
            for (int i = 0; i < h; i++) {
                sr.line(i * Utils.TILE_HEIGHT, 0, i * Utils.TILE_HEIGHT, w * Utils.TILE_WIDTH);
            }
            // draw selection rect
            sr.setColor(1.0f, 0.0f, 0.0f, 1.0f);
            sr.rect(selectRect.getX(), selectRect.getY(), selectRect.getWidth(), selectRect.getHeight());
            sr.end();

            if (fpsSecond > 1.0f) {
                fps = frameCount;
                frameCount = 0;
                fpsSecond = 0.0f;
            }
            frameCount++;
            fpsSecond += delta;
            hudBatch.begin();
            fontFPS.draw(hudBatch, "FPS: " + fps, 10.0f, 70.0f);
            hudBatch.end();
        }
    }

    @Override
    public void resize(int width, int height) {
        screenWidth = width;
        screenHeight = height;
        hud.resize(width, height);
        // Viewport relative to tile width and initial screen width.  Shows 16 tiles accross.
        float viewportTiles = (Utils.TILE_WIDTH * 20.0f) / initScreenWidth;
        camera.viewportWidth = viewportTiles * screenWidth;
        camera.viewportHeight = viewportTiles * screenHeight;
        camera.update();

        // Debug stuff
        if (fontFPS != null) {
            float scaleFontFPS = screenWidth / 800.0f * 2.0f;
            fontFPS.getData().setScale(scaleFontFPS);
        }
    }

    @Override
    public void pause() {
        String checker = "Make changes here";
        Preferences pref = Gdx.app.getPreferences(Utils.PREF_LEVEL_STATE);
        pref.clear();
        pref.flush();
        pref.putInteger(Utils.VAL_CURRENT_TURN, level.getRoundManager().getCurTurn());
        pref.putInteger(Utils.VAL_PLAYER_X, (int) level.getPlayer().getX());
        pref.putInteger(Utils.VAL_PLAYER_Y, (int) level.getPlayer().getY());
        pref.putInteger(Utils.VAL_PLAYER_HEALTH, level.getPlayer().getHealth());

        int i = 0;
        for (Avatar e : level.getEnemies()) {
            pref.putInteger("enemyPosX" + i, e.getGridX());
            pref.putInteger("enemyPosY" + i, e.getGridY());
            pref.putInteger("enemyHealth" + i, e.getHealth());
            i++;
        }
        i = 0;
        for (Avatar d : level.getDeadEnemies()) {
            pref.putInteger("deadPosX" + i, d.getGridX());
            pref.putInteger("deadPosY" + i, d.getGridY());
            i++;
        }
        pref.putInteger(Utils.VAL_ALIVE_ENEMIES, i);
        i = 0;
        for (Interactive ia : level.getInteractives()) {
            if (ia instanceof Door) {
                pref.putBoolean("Door" + i, ((Door) ia).isOpen());
                pref.putBoolean("isDoorLocked" + i, ((Door) ia).isLocked());
            } else if (ia instanceof StepTrap)
                pref.putBoolean("StepTrap" + i, ((StepTrap) ia).isActive());

            i++;
        }
        pref.putBoolean("cage_door", level.getCageDoor().isOpen());

        i = 0;
        for (InventoryItem item : level.getPlayer().getInventory()) {
            pref.putFloat("pItemX" + i, item.getX());
            pref.putFloat("pItemY" + i, item.getY());
            i++;
        }
        pref.putInteger(Utils.VAL_PLAYER_INVENTORY_ITEM_NUM, i);
        i = 0;
        for (InventoryItem item : level.getItems()) {
            pref.putString("itemType" + i, item.getType().toString());
            pref.putFloat("itemX" + i, item.getX());
            pref.putFloat("itemY" + i, item.getY());
            i++;
        }
        pref.putInteger(Utils.VAL_ITEM_NUM, i);

        pref.putInteger(Utils.VAL_CURRENT_SCREEN, Utils.SCREEN_GAME);
        pref.putInteger(Utils.VAL_CURRENT_LEVEL, levelNumber);

        pref.flush();
    }

    @Override
    public void resume() {
        String checker = "";
    }

    @Override
    public void hide() {
        String Checker = "";
    }

    @Override
    public void dispose() {
        hudBatch.dispose();
        renderer.dispose();
    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        switch (keycode) {
            case Input.Keys.D:
                toggleDebug();
                break;
            case Input.Keys.R:
                restart();
                break;
        }
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        // Convert screen pos to in-game pos.
        touchDownPos = camera.unproject(new Vector3(screenX, screenY, 0));
        initialZoom = camera.zoom;
        handledTouch = false;
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        if (handledTouch) {
            return false;
        }
        // Convert screen pos to in-game pos.
        Vector3 pos = camera.unproject(new Vector3(screenX, screenY, 0));
        // If tap out of bounds
        if (pos.x < 0 || pos.y < 0
                || pos.x > level.getMapPixelWidth()
                || pos.y > level.getMapPixelHeight()) {
            return false;
        }
        // Get grid position
        Vector2 gridPos = level.getSnappedPosition(pos.x, pos.y);
        if (debug) {
            selectRect.setPosition(gridPos);
            Utils.logD("touchUp(" + gridPos.x + "," + gridPos.y + ")");
        }
        handledTouch = level.selectCell(gridPos);
        return handledTouch;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
//        Vector3 pos = camera.unproject(new Vector3(screenX, screenY, 0));
//        Vector3 diffPos = new Vector3(touchDownPos.x - pos.x, touchDownPos.y - pos.y, 0);
//        camera.position.set(camera.position.x + diffPos.x, camera.position.y + diffPos.y, 0);
//        camera.update();
//        handledTouch = true;
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        if (debug) {
            // Convert screen pos to in-game pos.
            Vector3 pos = camera.unproject(new Vector3(screenX, screenY, 0));
            // Get grid position
            Vector2 gridPos = level.getSnappedPosition(pos.x, pos.y);
            TiledMapTile t = level.getTile((int) (gridPos.x / Utils.TILE_WIDTH), (int) (gridPos.y / Utils.TILE_HEIGHT));
            if (t != null) {
                selectRect.setPosition(gridPos);
            }
        }
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        float newZoom = camera.zoom;
        if (amount > 0)
            newZoom *= ZOOM_INC;
        else if (amount < 0)
            newZoom /= ZOOM_INC;
        setCameraZoom(newZoom);
        return false;
    }

    @Override
    public boolean touchDown(float x, float y, int pointer, int button) {
        return false;
    }

    @Override
    public boolean tap(float x, float y, int count, int button) {
        return false;
    }

    @Override
    public boolean longPress(float x, float y) {
        if (DemiDungeonsGame.devmode) {
            if (x < screenWidth / 2) {
                toggleDebug();
            } else if (x > screenWidth / 2) {
                restart();
            }
            handledTouch = true;
        }
        return handledTouch;
    }

    @Override
    public boolean fling(float velocityX, float velocityY, int button) {
        return false;
    }

    @Override
    public boolean pan(float x, float y, float deltaX, float deltaY) {
        return false;
    }

    @Override
    public boolean panStop(float x, float y, int pointer, int button) {
        return false;
    }

    @Override
    public boolean zoom(float initialDistance, float distance) {
        float delta = initialDistance - distance;
        float ratio = Math.abs(delta) / initialDistance;
        Utils.logD("zoom: I:" + initialDistance + " D:" + distance + " Delta:" + delta);
        float newZoom = initialZoom;
        float zoomInc = (ratio * 2.0f) + 1.0f;
        if (delta > 0)
            newZoom *= zoomInc;
        else if (delta < 0)
            newZoom /= zoomInc;
        setCameraZoom(newZoom);
        handledTouch = true;
        return handledTouch;
    }

    @Override
    public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2, Vector2 pointer1, Vector2 pointer2) {
//        Utils.log("pinch");
//        Utils.log("  I1: " + initialPointer1);
//        Utils.log("  I2: " + initialPointer2);
//        Utils.log("  P1: " + pointer1);
//        Utils.log("  P2: " + pointer2);
        return false;
    }

    public int getLevelNumber() {
        return levelNumber;
    }

    private void setCameraZoom(float newZoom) {
        if (newZoom > 0.2f && newZoom < 5.0f) {
            camera.zoom = newZoom;
            if (debug) {
                Utils.logD("newZoom: " + newZoom);
            }
        }
    }

    public void toggleDebug() {
        debug = !debug;
        Utils.logD("DEBUG: " + (debug ? "ON" : "OFF"));
    }

    public void restart() {
        DemiDungeonsGame.game.setScreen(new GameScreen(levelNumber, false));
        Utils.logD("RESTART");
    }

    private void updateProgressData() {
        if (level.isGameFinished()) {
            Preferences pref = Gdx.app.getPreferences(Utils.PREF_SAVE_FILE);
            int latestLevel = pref.getInteger(Utils.VAL_LATEST_LEVEL, 1);
            if (levelNumber > latestLevel && levelNumber <= Utils.HIGHEST_LEVEL) {
                pref.putInteger(Utils.VAL_LATEST_LEVEL, levelNumber);
                pref.flush();
            }
        }
    }
}
