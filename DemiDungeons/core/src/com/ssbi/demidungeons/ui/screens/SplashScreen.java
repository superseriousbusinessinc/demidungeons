package com.ssbi.demidungeons.ui.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.ssbi.demidungeons.DemiDungeonsGame;

/**
 * Created by alpha_000 on 2015-09-25.
 */
public class SplashScreen implements Screen {
    private static final double TIME_TO_FADE = 2.0;

    private SpriteBatch batch;
    private Texture texSplash;
    private float stateTime;
    private float alpha;

    private boolean screenChanged;

    public SplashScreen() {
    }

    @Override
    public void show() {
        batch = new SpriteBatch();
        texSplash = new Texture(Gdx.files.internal("images/ssblogo.png"));
        stateTime = 0.0f;
        alpha = 1.0f;
        screenChanged = false;

        InputAdapter adapter = new InputAdapter() {
            @Override
            public boolean touchUp(int screenX, int screenY, int pointer, int button) {
                screenChanged = true;
                DemiDungeonsGame.game.setScreen(new MainMenu());
                return true;
            }
        };
        Gdx.input.setInputProcessor(adapter);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        // Code to slowly fade out logo.
        stateTime += delta;
        if (stateTime > 2.0f) {
            alpha = 2.0f - (float) (stateTime / TIME_TO_FADE);
            if (alpha < 0.0f) alpha = 0.0f;
        }

        batch.begin();
        batch.draw(texSplash, 0.0f, 0.0f, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        batch.setColor(1.0f, 1.0f, 1.0f, alpha);
        batch.end();
        if(stateTime > 4.0f && !screenChanged) {
            DemiDungeonsGame.game.setScreen(new MainMenu());
        }
    }

    @Override
    public void hide() {
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void dispose() {
        texSplash.dispose();
        batch.dispose();
    }
}
