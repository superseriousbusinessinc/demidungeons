package com.ssbi.demidungeons.effects;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.ssbi.demidungeons.utils.TextureManager;
import com.ssbi.demidungeons.utils.Utils;

/**
 * Created by Benjamin on 2015-11-11.
 */
public class OverlayEffect extends Sprite {

    protected enum State {
        VISIBLE,
        HIDDEN,
        FADE_IN,
        FADE_OUT
    }

    protected TextureRegion tex;
    protected float alpha;
    protected Vector2 pos;

    protected State state;
    protected float stateTime;
    protected float showDuration;
    protected boolean fadeOut;

    protected float fadeInDuration;
    protected float fadeOutDuration;

    public OverlayEffect() {
        super();
        state = State.HIDDEN;
        fadeInDuration = 0.5f;
        fadeOutDuration = 0.5f;
        stateTime = 0.0f;
        alpha = 0.0f;
        pos = new Vector2();
        setType(Utils.EffectType.DAMAGE); // default effect
    }

    public OverlayEffect(Utils.EffectType effectType) {
        this();
        setType(effectType);
    }

    public void setType(Utils.EffectType effectType) {
        tex = TextureManager.getEffectTexture(effectType);
    }

    public void update(float delta) {
        stateTime += delta;
        switch (state) {
            case FADE_IN:
                alpha = ((stateTime) / fadeOutDuration);
                if (fadeInDuration <= stateTime) {
                    setVisible(true);
                    stateTime = 0;
                }
                break;
            case VISIBLE:
                if (showDuration < stateTime) {
                    if (fadeOut) {
                        state = State.FADE_OUT;
                        alpha = 1.0f;
                    } else {
                        setVisible(false);
                    }
                    stateTime = 0;
                }
                break;
            case FADE_OUT:
                alpha = ((fadeOutDuration - stateTime) / fadeOutDuration);
                if (fadeOutDuration <= stateTime) {
                    setVisible(false);
                    stateTime = 0;
                }
                break;
        }
    }

    public void draw(Batch batch) {
        if(state != State.HIDDEN) {
            Color c = batch.getColor();
            batch.setColor(1.0f, 1.0f, 1.0f, alpha);
            batch.draw(tex, pos.x, pos.y);
            batch.setColor(c);
        }
    }

    public void setPosition(float x, float y) {
        pos.set(x, y);
    }

    public void setVisible(boolean isVisible) {
        state = isVisible ? State.VISIBLE : State.HIDDEN;
        alpha = isVisible ? 1.0f : 0.0f;
    }

    public void setFadeDuration(float fadeDuration) {
        fadeInDuration = fadeDuration;
        fadeOutDuration = fadeDuration;
    }

    public void show(float seconds) {
        show(seconds, false, false);
    }

    public void show(float seconds, boolean fadeIn, boolean fadeOut) {
        if (fadeIn) {
            state = State.FADE_IN;
            alpha = 0.0f;
        } else {
            setVisible(true);
        }
        showDuration = seconds;
        this.fadeOut = fadeOut;
        stateTime = 0.0f;
    }
}
