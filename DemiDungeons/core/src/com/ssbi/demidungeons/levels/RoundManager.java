package com.ssbi.demidungeons.levels;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.ssbi.demidungeons.objects.Avatar;
import com.ssbi.demidungeons.utils.Utils;

import java.util.List;

/**
 * Created by Benjamin on 2015-11-08.
 */
public class RoundManager {

    private List<Avatar> avatars;
    private int curTurn;

    private int roundCount;

    public RoundManager(List<Avatar> avatars) {
        this.avatars = avatars;
        curTurn = -1; // Start at -1 so the first time you call next turn, you get the first avatar in the list.
        roundCount = 0;
    }

    public Avatar getNextTurn() {
        curTurn++;
        if(curTurn >= avatars.size() || curTurn < 0) {
            curTurn = 0;
            roundCount++;
        }
        return avatars.get(curTurn);
    }

    public int getCurTurn(){
        return curTurn;
    }

    public void setCurTurn(int turn){
        curTurn = turn;
    }

    public int getCurrentRound() {
        return roundCount;
    }
}
