package com.ssbi.demidungeons.levels;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.math.Vector2;
import com.ssbi.demidungeons.DemiDungeonsGame;
import com.ssbi.demidungeons.objects.Avatar;
import com.ssbi.demidungeons.objects.InventoryItem;
import com.ssbi.demidungeons.objects.WorldComponent;
import com.ssbi.demidungeons.objects.characters.Player;
import com.ssbi.demidungeons.objects.characters.Rat;
import com.ssbi.demidungeons.objects.characters.RatQueen;
import com.ssbi.demidungeons.objects.interactive.Door;
import com.ssbi.demidungeons.objects.interactive.Interactive;
import com.ssbi.demidungeons.objects.interactive.StepTrap;
import com.ssbi.demidungeons.ui.screens.MainMenu;
import com.ssbi.demidungeons.utils.TextureManager;
import com.ssbi.demidungeons.utils.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Benjamin on 2015-10-23.
 */
public class GameLevel {

    String levelGroup;
    int levelNumber;

    TiledMap map;
    TiledMapTileLayer mainLayer;

    Avatar player;

    List<Avatar> enemies;
    List<Avatar> dead;
    List<InventoryItem> items;
    List<Interactive> interactives;

    RoundManager roundManager;
    Avatar curTurn;

    ArrayList<ArrayList<ArrayList<WorldComponent>>> gridObjs;

    Door cageDoor;
    boolean isGameFinished = false;

    InventoryItem selItem;

    public GameLevel(String levelGroup, int levelNumber) throws Exception {
        //Load in Tiled Map
        this.levelGroup = levelGroup;
        this.levelNumber = levelNumber;
        String mapFilePath = "maps/" + this.levelGroup + "/" + this.levelNumber + ".tmx";
        FileHandle tmxHandle = Gdx.files.internal(mapFilePath);
        if(!tmxHandle.exists()) {
            DemiDungeonsGame.game.setScreen(new MainMenu());
        }

        map = new TmxMapLoader().load(mapFilePath);
        mainLayer = (TiledMapTileLayer) map.getLayers().get("main");
        if (mainLayer.getTileWidth() != Utils.TILE_WIDTH && mainLayer.getTileHeight() != Utils.TILE_HEIGHT) {
            throw new Exception("Tile dimensions must be 16x16 pixels.");
        }
        createObjectGrid();

        // Init object arrays
        enemies = new ArrayList<Avatar>();
        items = new ArrayList<InventoryItem>();
        interactives = new ArrayList<Interactive>();

        dead = new ArrayList<Avatar>();

        // Load map objects
        loadMapSpawns();
        loadMapItems();
        loadMapInterativeObjects();

        ArrayList<Avatar> avatarList = new ArrayList<Avatar>();
        avatarList.add(player);
        for (Avatar a : enemies) {
            avatarList.add(a);
        }
        roundManager = new RoundManager(avatarList);
        curTurn = roundManager.getNextTurn();
        curTurn.beginTurn();
    }

    public void update(float delta) {
        if (curTurn.isTurnFinished()) {
            curTurn = roundManager.getNextTurn();
            curTurn.beginTurn();
        } else if (curTurn != player) {
            // Do player stuff.
            curTurn.doTurn(this);
        }

        for (Avatar d : dead) {
            d.update(delta);
        }
        for (Avatar enemy : enemies) {
            enemy.update(delta);
        }
        for (Interactive inter : interactives) {
            if (inter instanceof StepTrap) {
                if (player.getX() == inter.getX() && player.getY() == inter.getY()) {
                    inter.onContact(player);
                } else {
                    for (Avatar e : enemies) {
                        if (e.getX() == inter.getX() && e.getY() == inter.getY()) {
                            inter.onContact(e);
                        }
                    }
                }
            }
        }
        player.update(delta);
        if (player.getX() == cageDoor.getX() && player.getY() == cageDoor.getY()) {
            isGameFinished = true;
        }

        // Clean up dead enemies
        for (int i = enemies.size() - 1; i >= 0; i--) {
            if(enemies.get(i).getState() == Avatar.State.DEAD) {
                dead.add(enemies.get(i));
                enemies.remove(i);
            }
        }
    }

    public void setDeadEnemies(int deadEnemies){
        for (int i = deadEnemies - 1; i >= 0; i--) {
            enemies.get(i).setState(Avatar.State.DEAD);
            dead.add(enemies.get(i));
            enemies.remove(i);
        }
    }

    public void draw(Batch batch) {
        for (Interactive inter : interactives) {
            inter.draw(batch);
        }
        for (Avatar d : dead) {
            d.draw(batch);
        }
        for (InventoryItem item : items) {
            item.draw(batch);
        }
        cageDoor.draw(batch);
        for (Avatar enemy : enemies) {
            enemy.draw(batch);
        }
        player.draw(batch);
    }


    /**********************************
     * Getter Helpers                 *
     **********************************/

    public String getLevelGroup() {
        return levelGroup;
    }

    public int getLevelNumber() {
        return levelNumber;
    }

    public TiledMap getMap() {
        return map;
    }

    public TiledMapTileLayer getMainLayer() {
        return mainLayer;
    }

    public float getMapPixelWidth() {
        return mainLayer.getWidth() * Utils.TILE_WIDTH;
    }

    public float getMapPixelHeight() {
        return mainLayer.getHeight() * Utils.TILE_HEIGHT;
    }

    public RoundManager getRoundManager(){return roundManager;}

    public Vector2 getSnappedPosition(float x, float y) {
        return new Vector2(((int) x) - ((int) x) % Utils.TILE_WIDTH, ((int) y) - ((int) y) % Utils.TILE_HEIGHT);
    }

    public void setDestination(Vector2 gridPos) {
        player.setWalkDestination(gridPos, mainLayer);

//        for (Avatar enemy : enemies) {
//            enemy.setWalkDestination(new Vector2(player.getX(), player.getY()), mainLayer);
//        }
    }

    public TiledMapTileLayer.Cell getCell(int x, int y) {
        return mainLayer.getCell(x, y);
    }

    public TiledMapTile getTile(int x, int y) {
        TiledMapTileLayer.Cell cell = mainLayer.getCell(x, y);
        return (cell == null ? null : cell.getTile());
    }

    public Avatar getPlayer() {
        return player;
    }

    public Avatar getCurrentTurn(){return curTurn;}

    public List<Avatar> getEnemies() {
        return enemies;
    }

    public List<Avatar> getDeadEnemies() {
        return dead;
    }

    public List<Interactive> getInteractives(){
        return interactives;
    }

    // Player actions based on where the user clicked
    public boolean selectCell(Vector2 snappedPos) {
        // Can only move during the player's turn.
        if (curTurn != player || player.getState() != Avatar.State.IDLE || player.isTurnFinished()) {
            return false;
        }

        float snappedX = snappedPos.x;
        float snappedY = snappedPos.y;
        if(snappedX > player.getX()) {
            player.setFacingRight(true);
        } else if(snappedX < player.getX()) {
            player.setFacingRight(false);
        }
        // Get selected component
        Interactive selInter = getComponentOnCell(snappedX, snappedY, interactives);
        selItem = getComponentOnCell(snappedX, snappedY, items);
        Avatar selEnemy = getComponentOnCell(snappedX, snappedY, enemies);
        WorldComponent comp = null;
        if (selEnemy != null)
            comp = selEnemy;
        else if (selItem != null) {
            comp = selItem;
        } else if (selInter != null) {
            comp = selInter;
        }
        if (cageDoor.getX() == snappedX && cageDoor.getY() == snappedY) {
            comp = cageDoor;
        }

        if (player.getX() == snappedX && player.getY() == snappedY) { // Check if player is on cell
            if (selItem != null && comp == selItem) { // Pick up item if it is not null
                player.addInventoryItem(selItem);
                items.remove(selItem);
                player.decreaseTurn(1);
                return true;
            } else {
                return false;
            }
        } else if (isBesidePlayer((int) snappedX, (int) snappedY)) { // Check if player is beside the cell
            if (comp instanceof Avatar) {
                if(((Avatar) comp).getState() != Avatar.State.DEAD) {
                    ((Avatar) comp).damaged(10);
                    player.decreaseTurn(1);
                    return true;
                }
            } else if (comp instanceof Door) {
                Door d = (Door) comp;
                InventoryItem unlockItem = player.getInventoryItem(d.getUnlockKeyType());
                if (d.isLocked() && unlockItem != null) {
                    player.removeInventoryItem(unlockItem);
                    d.setLocked(false);
                    if (d == cageDoor) {
                        d.setOpen(true);
                    }
                    player.decreaseTurn(1);
                    return true;
                }
                if (!d.isOpen()) {
                    d.setOpen(true);
                    player.decreaseTurn(1);
                    return true;
                }
            }
        }
        // Walk collision check for player.
        Vector2 dir = new Vector2(snappedX - player.getX(), snappedY - player.getY());
        Vector2 walkDest = new Vector2(player.getX(), player.getY());
        if (dir.x > 0)
            walkDest.x += Utils.TILE_WIDTH;
        else if (dir.x < 0)
            walkDest.x -= Utils.TILE_WIDTH;
        if (dir.y > 0)
            walkDest.y += Utils.TILE_HEIGHT;
        else if (dir.y < 0)
            walkDest.y -= Utils.TILE_HEIGHT;
        if (!hasCollision(walkDest.x, walkDest.y)) {
            setDestination(walkDest);
            return true;
        }
        return false;
    }

    public boolean isBesidePlayer(int x, int y) {
        float pX = player.getX();
        float pY = player.getY();
        if (pX == x) {
            if ((pY + Utils.TILE_HEIGHT) == y) { // top
                return true;
            } else if ((pY - Utils.TILE_HEIGHT) == y) { // bottom
                return true;
            }
        } else if (pY == y) {
            if ((pX + Utils.TILE_WIDTH) == x) { // right
                return true;
            } else if ((pX - Utils.TILE_WIDTH) == x) { // left
                return true;
            }
        } else {
            if ((pX + Utils.TILE_WIDTH) == x && (pY + Utils.TILE_HEIGHT) == y) { // top right
                return true;
            } else if ((pX - Utils.TILE_WIDTH) == x && (pY + Utils.TILE_HEIGHT) == y) { // top left
                return true;
            } else if ((pX + Utils.TILE_WIDTH) == x && (pY - Utils.TILE_HEIGHT) == y) { // bottom right
                return true;
            } else if ((pX - Utils.TILE_WIDTH) == x && (pY - Utils.TILE_HEIGHT) == y) { // bottom left
                return true;
            }
        }
        return false;
    }

    public boolean hasCollision(float snappedX, float snappedY) {
        TiledMapTile t = getTile((int) (snappedX / Utils.TILE_WIDTH), (int) (snappedY / Utils.TILE_HEIGHT));
        if (t != null && t.getProperties().containsKey("collider")) {
            return true;
        }
        for (Avatar e : enemies) {
            if (e.isCollider() && e.getX() == snappedX && e.getY() == snappedY) {
                return true;
            }
        }
        for (Interactive i : interactives) {
            if (i.isCollider() && i.getX() == snappedX && i.getY() == snappedY) {
                return true;
            }
        }
        if (player.getX() == snappedX && player.getY() == snappedY) {
            return true;
        }
        return false;
    }

    public void dropInventory(Avatar avatar) {
        List<InventoryItem> inv = avatar.getInventory();
        float x = avatar.getX();
        float y = avatar.getY();
        for(int i = inv.size() - 1; i >= 0; i--) {
            InventoryItem item = inv.get(i);
            item.setPosition(x,y);
            items.add(item);
            inv.remove(item);
        }
    }

    public Door getCageDoor(){
        return cageDoor;
    }

    public boolean isGameFinished() {
        return isGameFinished;
    }

    /**********************************
     * Private Methods                *
     **********************************/

    // Create Object Grid
    private void createObjectGrid() {
        // TODO: Create an object grid that will be used to keep try of the location of everything in the grid.
        gridObjs = new ArrayList<ArrayList<ArrayList<WorldComponent>>>(mainLayer.getWidth());
        for (int x = 0; x < gridObjs.size(); x++) {
            ArrayList<ArrayList<WorldComponent>> col = new ArrayList<ArrayList<WorldComponent>>(mainLayer.getHeight());
            gridObjs.add(x, col);
            for (int y = 0; y < col.size(); y++) {
                ArrayList<WorldComponent> tmp = new ArrayList<WorldComponent>();
                col.add(y, tmp);
            }
        }
    }

    // Load Map Avatar Spawns
    private void loadMapSpawns() throws Exception {
        float x = 0.0f;
        float y = 0.0f;
        float w = 0.0f;
        float h = 0.0f;
        for (MapObject obj : map.getLayers().get("spawn").getObjects()) {
            MapProperties mps = obj.getProperties();
            x = Float.parseFloat(mps.get("x").toString());
            y = Float.parseFloat(mps.get("y").toString());
            w = Float.parseFloat(mps.get("width").toString());
            h = Float.parseFloat(mps.get("height").toString());
            Object prop;
            if (mps.containsKey("player")) {
                initPlayer(mps, x, y, w, h);
            } else if (mps.containsKey("enemy")) {
                initEnemy(mps, x, y, w, h);
            }
        }
        if (player == null) {
            throw new Exception("Could not find player spawn in map.");
        }
    }

    private void initPlayer(MapProperties mps, float x, float y, float w, float h) {
        Object prop = mps.get("player");
        Utils.AvatarType playerType = null;
        if (prop.equals("warrior")) {
            playerType = Utils.AvatarType.P_WARRIOR;
        } else if (prop.equals("mage")) {
            playerType = Utils.AvatarType.P_MAGE;
        } else if (prop.equals("rogue")) {
            playerType = Utils.AvatarType.P_ROGUE;
        } else if (prop.equals("hunter")) {
            playerType = Utils.AvatarType.P_HUNTER;
        }
        player = new Player(this, playerType);
        player.setPosition(x, y + Utils.TILE_HEIGHT);
        player.setSize(w, h);
        prop = mps.get("inventory");
        if(prop != null) {
            String[] strInv = prop.toString().split(",");
            parseInventoryItems(player, strInv);
        }
    }

    private void initEnemy(MapProperties mps, float x, float y, float w, float h) {
        Object prop = mps.get("enemy");
        Avatar enemy = null;
        if (prop.equals("ratqueen")) {
            enemy = new RatQueen(this);
            enemy.setPosition(x, y);
            enemy.setSize(w, h);
        }
        else  if (prop.equals("rat")) {
            enemy = new Rat(this);
            enemy.setPosition(x, y);
            enemy.setSize(w, h);
        }
        prop = mps.get("inventory");
        if(prop != null) {
            String[] strInv = prop.toString().split(",");
            parseInventoryItems(enemy, strInv);
        }
        if (enemy != null)
            enemies.add(enemy);
    }

    private void parseInventoryItems(Avatar avatar, String[] strInv) {
        for(int i = 0; i < strInv.length; i++) {
            String itm = strInv[i];
            Utils.ItemType t = null;
            if(itm.equals("KEY_0")) {
                t = Utils.ItemType.KEY_0;
            }
            else if(itm.equals("KEY_1")) {
                t = Utils.ItemType.KEY_1;
            }
            else  if(itm.equals("POTION_2")) {
                t = Utils.ItemType.POTION_2;
            }
            if(t != null) {
                InventoryItem item = new InventoryItem(t);
                item.setPosition(0, 0);
                item.setSize(Utils.TILE_WIDTH, Utils.TILE_HEIGHT);
                avatar.getInventory().add(item);
            }
        }
    }

    // Load Map Items
    private void loadMapItems() throws Exception {
        float x = 0.0f;
        float y = 0.0f;
        float w = 0.0f;
        float h = 0.0f;
        for (MapObject obj : map.getLayers().get("item").getObjects()) {
            MapProperties mps = obj.getProperties();
            x = Float.parseFloat(mps.get("x").toString());
            y = Float.parseFloat(mps.get("y").toString());
            w = Float.parseFloat(mps.get("width").toString());
            h = Float.parseFloat(mps.get("height").toString());
            if (mps.containsKey("key")) {
                initKey(mps, x, y, w, h);
            }
            else  if (mps.containsKey("potion")) {
                initPotion(mps, x, y, w, h);
            }
        }
    }

    private void initKey(MapProperties mps, float x, float y, float w, float h) {
        Object prop = mps.get("key");
        if (prop.equals("0")) {
            InventoryItem item = new InventoryItem(Utils.ItemType.KEY_0);
            item.setPosition(x, y + Utils.TILE_HEIGHT);
            item.setSize(w, h);
            items.add(item);
        }
        else if (prop.equals("1")) {
            InventoryItem item = new InventoryItem(Utils.ItemType.KEY_1);
            item.setPosition(x, y + Utils.TILE_HEIGHT);
            item.setSize(w, h);
            items.add(item);
        }
    }

    private void initPotion(MapProperties mps, float x, float y, float w, float h) {
        Object prop = mps.get("potion");
        if (prop.equals("2")) {
            InventoryItem item = new InventoryItem(Utils.ItemType.POTION_2);
            item.setPosition(x, y + Utils.TILE_HEIGHT);
            item.setSize(w, h);
            items.add(item);
        }

    }

    // Load Interactive objects
    private void loadMapInterativeObjects() throws Exception {
        float x = 0.0f;
        float y = 0.0f;
        float w = 0.0f;
        float h = 0.0f;
        for (MapObject obj : map.getLayers().get("interactive").getObjects()) {
            MapProperties mps = obj.getProperties();
            x = Float.parseFloat(mps.get("x").toString());
            y = Float.parseFloat(mps.get("y").toString());
            w = Float.parseFloat(mps.get("width").toString());
            h = Float.parseFloat(mps.get("height").toString());
            if (mps.containsKey("door")) {
                initDoor(mps, x, y, w, h);
            } else if (mps.containsKey("step_trap")) {
                initStepTrap(mps, x, y, w, h);
            }
        }
        if (cageDoor == null) {
            throw new Exception("Could not find skeleton door in map.");
        }
    }

    private void initDoor(MapProperties mps, float x, float y, float w, float h) {
        String prop = mps.get("door").toString();
        if (prop.contains("0")) {
            // Load cage door textures.
            TextureRegion locked = new TextureRegion(
                    TextureManager.getTilesetTexture(),
                    Utils.TILE_WIDTH_INT * 9,
                    Utils.TILE_HEIGHT_INT * 1,
                    Utils.TILE_WIDTH_INT,
                    Utils.TILE_HEIGHT_INT
            );
            cageDoor = new Door(locked, locked);
            cageDoor.setLocked(true);
            cageDoor.setUnlockKeyType(Utils.ItemType.KEY_0);
            cageDoor.setPosition(x, y + Utils.TILE_HEIGHT);
            cageDoor.setSize(w, h);
        } else if (prop.contains("1")) {
            Door door = new Door();
            door.setPosition(x, y + Utils.TILE_HEIGHT);
            door.setSize(w, h);
            door.setUnlockKeyType(Utils.ItemType.KEY_1);
            if (prop.contains("unlocked")) {
                door.setLocked(false);
            }else if (prop.contains("locked")){
                door.setLocked(true);
            }
            interactives.add(door);
        }
    }

    private <T extends WorldComponent> T getComponentOnCell(float snappedX, float snappedY, List<T> compArr) {
        T comp = null;
        for (T c : compArr) {
            if (c.getX() == snappedX && c.getY() == snappedY) {
                comp = c;
                break;
            }
        }
        return comp;
    }

    private void initStepTrap(MapProperties mps, float x, float y, float w, float h) {
        String prop = mps.get("step_trap").toString();
        boolean isActive = true;
        if (mps.containsKey("isActive")) {
            String state = mps.get("isActive").toString();
            isActive = Boolean.parseBoolean(state);
        }
        int trapID = Integer.parseInt(prop);
        StepTrap trap = new StepTrap(trapID, isActive);
        trap.setPosition(x, y + Utils.TILE_HEIGHT);
        trap.setSize(w, h);
        interactives.add(trap);
    }

    public List<InventoryItem> getItems()
    {
        return items;
    }

    public void removeItem(InventoryItem item)
    {
        items.remove(item);
    }

    public void setInventoryItem(float x, float y) {
        selItem = getComponentOnCell(x, y, items);
        player.addInventoryItem(selItem);
        items.remove(selItem);
    }
}
