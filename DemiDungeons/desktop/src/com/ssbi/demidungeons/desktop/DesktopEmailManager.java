package com.ssbi.demidungeons.desktop;

import com.ssbi.demidungeons.utils.EmailManager;

import java.awt.Desktop;
import java.net.URI;

/**
 * Created by Benjamin on 2015-12-02.
 */
public class DesktopEmailManager extends EmailManager {
    @Override
    public void sendEmail(String sender, String receiver, String subject, String message) {
        try {
            Desktop desktop = Desktop.getDesktop();
            String msg = "mailto:" + receiver + "?subject=" + subject;
            URI uri = URI.create(msg);
            desktop.mail(uri);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
