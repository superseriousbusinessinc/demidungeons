package com.ssbi.demidungeons.desktop;

import com.badlogic.gdx.Files;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.ssbi.demidungeons.DemiDungeonsGame;
import com.ssbi.demidungeons.utils.DemiDungeonsConfig;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = "Demi Dungeons";
		config.addIcon("icons/dd_icon_32.png", Files.FileType.Internal);
		config.width = 960;
		config.height = 540;
		DemiDungeonsConfig ddConfig = new DemiDungeonsConfig();
		ddConfig.emailManager = new DesktopEmailManager();
		new LwjglApplication(new DemiDungeonsGame(ddConfig), config);
	}
}
